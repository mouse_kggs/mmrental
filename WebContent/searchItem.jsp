<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@
	page import="test.ItemDTO, java.util.ArrayList, java.sql.Date"
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/searchpage.js"></script>
<title>MMR レンタルサイト</title>

<style type="text/css">
	<!--
		th{text-align: center;}
		.center{text-align: center;}
		td{vartical-align:middle;}
		
		.square_btn{
				    position: relative;
				    display: inline-block;
				    font-weight: bold;
				    padding: 0.25em 0.5em;
				    text-decoration: none;
				    color: #00BCD4;
				    background: #ECECEC;
				    transition: .4s;
				  }
				
				.square_btn:hover {
				    background: #00bcd4;
				    color: white;
				}
	-->
</style>
</head>
<body>
	<jsp:include page="header_common.jsp" flush="true" />
<br>
<div>検索結果一覧</div>
<c:if test="${empty searchResult}"><font color="red">見つかりませんでした。</font></c:if>
<c:out value="${CartError}"/>
<c:if test="${!empty searchResult}">
<table class="table table-striped">
	<tr>
		<th></th>
		<th>新旧</th>
		<th>カテゴリ</th>
		<th>商品画像</th>
		<th>タイトル</th>
		<th>アーティスト</th>
		<th>値段</th>
		<th></th>
	</tr>
	<c:forEach var="data" items="${searchResult}" varStatus="status">
		<tr>
			<td style="vertical-align:middle"><c:out value="${((page - 1) * 10) + status.index + 1}"/></td>
			<td class="center" style="vertical-align:middle"><c:out value="${data.new_and_old_name}"/></td>
			<td class="center" style="vertical-align:middle"><c:out value="${data.category_name}"/></td>
			<td class="center" style="vertical-align:middle"><a href='itemDetail?item_id=<c:out value="${data.item_id}"/>'><img style="width: 100px; height: 100px" src="image?id=${ data.item_id }"></a></td>
			<td style="vertical-align:middle"><a href='itemDetail?item_id=<c:out value="${data.item_id}"/>'><c:out value="${data.item_name}"/></a></td>
			<td style="vertical-align:middle"><c:out value="${data.artist}"/></td>
			<td class="center" style="vertical-align:middle">￥<c:out value="${data.price}"/></td>
			<td class="center" style="vertical-align:middle"><form action='addCart?item_id=<c:out value="${data.item_id}"/>' method="POST"><input type="submit" value="カートに追加" class="square_btn"></form></td>
		</tr>
	</c:forEach>
</table>
<font size="4"><center>
<c:if test="${(page-1) > 0}"><a href='paging?page=<c:out value="${page-1}"/>'>&lt; 前のページ</a></c:if> 

<c:if test="${(page-3) > 0}"><a href='paging?page=<c:out value="${page-3}"/>'><c:out value="${page-3}"/></a></c:if> 
<c:if test="${(page-2) > 0}"><a href='paging?page=<c:out value="${page-2}"/>'><c:out value="${page-2}"/></a></c:if> 
<c:if test="${(page-1) > 0}"><a href='paging?page=<c:out value="${page-1}"/>'><c:out value="${page-1}"/></a></c:if> 
<b><c:out value="${page}"/></b>
<c:if test="${(page+1) <= pageMax}"><a href='paging?page=<c:out value="${page+1}"/>'><c:out value="${page+1}"/></a></c:if> 
<c:if test="${(page+2) <= pageMax}"><a href='paging?page=<c:out value="${page+2}"/>'><c:out value="${page+2}"/></a></c:if> 
<c:if test="${(page+3) <= pageMax}"><a href='paging?page=<c:out value="${page+3}"/>'><c:out value="${page+3}"/></a></c:if> 

<c:if test="${(page+1) <= pageMax}"><a href='paging?page=<c:out value="${page+1}"/>'>次のページ &gt;</a></c:if> 
</center></font>
<br></c:if>
</body>
</html>