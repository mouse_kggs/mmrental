<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/home.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/home.js"></script>
<script type="text/javascript" src="js/searchpage.js"></script>
<title>MMR レンタルサイト</title>
</head>
<body>

<jsp:include page="header_common.jsp" flush="true" />
<br>
<%
	session.removeAttribute("returnTo");
	session.setAttribute("backTo", "homeView");
%>
<div class="panel panel-info">
	<div class="panel-heading">
		<h5 class="panel-title">
			<c:if test="${empty userProfile}">今日のおすすめ商品 pick up!</c:if>
			<c:if test="${!empty userProfile}">${userProfile.user_name}さんにおすすめ！</c:if>
		</h5>
	</div>
	<c:choose>
		<c:when test="${empty userProfile}">
			<div class="panel-body" style="height:600px;">
		</c:when>
		<c:otherwise>
			<div class="panel-body" style="height:300px;">
		</c:otherwise>
	</c:choose>
		<c:forEach var="data" items="${recommendItems}" varStatus="status">
			<c:if test="${status.index % 5 == 0}"><div class="col-xs-offset-1"></c:if>
			<div class="col-xs-2" style="padding-right:1px; padding-left:1px">
				<div class="panel panel-default" style="height:270px">
					<div class="panel-body" style="background-color:#ddffff">
						<div style="height: 200px">
							<div title='<c:out value="${data.item_name}"/>'>
								<center><a href='itemDetail?item_id=<c:out value="${data.item_id}"/>'><img style="max-width:100%; max-height:200px" src="image?id=${ data.item_id }"></a></center>
							</div>
						</div>
						<div>
							<div title='<c:out value="${data.item_name}"/>'>
								<a href='itemDetail?item_id=<c:out value="${data.item_id}"/>'><div class="over"><c:out value="${data.item_name}"/></div></a>
							</div>
							<div style="text-align:left; float:left"><c:out value="${data.category_name}"/></div>
							<div style="text-align:right">￥<c:out value="${data.price}"/></div>
						</div>
					</div>
				</div>
			</div>
			<c:if test="${status.index % 5 == 0}"> </div></c:if>
		</c:forEach>
	</div>
</div>

<c:if test="${!empty userProfile}">
	<div class="panel panel-warning">
		<div class="panel-heading">
			<h5 class="panel-title">過去に借りた商品 　　　<a href='rentalHistory' style="color:#0000ff">＞注文履歴を見る</a></h5>
		</div>
			<div class="col-xs-offset-1">
			<div class="panel-body" style="height:300px;">
			<c:forEach var="rdata" items="${rentalItems}" varStatus="status">
				<div class="col-xs-2" style="padding-right:1px; padding-left:1px">
					<div class="panel panel-default" style="height:270px">
						<div class="panel-body" style="background-color:#ffffdd">
							<div style="height: 200px">
								<div title='<c:out value="${rdata.item_name}"/>'>
									<center><a href='itemDetail?item_id=<c:out value="${rdata.item_id}"/>'><img style="max-width:100%; max-height:200px" src="image?id=${ rdata.item_id }"></a></center>
								</div>
							</div>
							<div>
								<div title='<c:out value="${rdata.item_name}"/>'>
									<a href='itemDetail?item_id=<c:out value="${rdata.item_id}"/>'><div class="over"><c:out value="${rdata.item_name}"/></div></a>
								</div>
								<div style="text-align:left; float:left"><c:out value="${rdata.category_name}"/></div>
								<div style="text-align:right">￥<c:out value="${rdata.price}"/></div>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>
			</div>
		</div>
	</div>
</c:if>
</body>
<jsp:include page="hooter.jsp" flush="true" />
</html>