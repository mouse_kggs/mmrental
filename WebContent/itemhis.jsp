<%@page import="java.util.ArrayList"%>
<%@page import="historyDTO.HisDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
	//使わない itemhis.jsp → rentalHistory.jspに変更しました
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="header2.jsp" flush="true" />
<title>注文履歴画面</title>
</head>
<body>
	<table>
		<h1 align="center">注文履歴</h1>
		<c:forEach var="list" items="${hisList1}">

			<c:if test="${list.RENTAL_NUMBER == data2.RENTAL_NUMBER}">
					<tr bgcolor="#ffffc0">
						<td align="center"><c:out value="${list.category_name}" /></td>
						<td align="center"><c:out value="${list.item_name}" /></td>
						<td align="center"><c:out value="${list.artist}" /></td>
						<td align="center">￥ <c:out value="${list.item_price}" /></td>
						<td align="center">数量:<c:out value="${list.item_count}" /></td>
					</tr>
			</c:if>
			<c:if test="${list.RENTAL_NUMBER != data2.RENTAL_NUMBER}">
				</table>
				<br>
				<table border="5" bordercolor="palevioletred" align="center" cellpadding="3">
					<tr bgcolor="#e3f0fb">
						<td height="45" width="100" align="center" valign="top"><b>レンタル日時</b><br> <br>
							<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
							<b><fmt:formatDate value="${list.ORDER_DATETIME}" pattern="yyyy/MM/dd HH:mm"/><br></b>
						</td>
						<td height="45" width="100" align="center" valign="top"><b>返却日</b><br> <br>
							<b><c:out value="${list.RETURN_DATETIME}" /></b>
						</td>
						<td height="90" width="100" align="center" valign="top"><b>数量</b><br> <br>
							<b><c:out value="${list.max}" /></b>
						</td>
						<td height="80" width="100" align="center" valign="top"><b>合計金額</b><br> <br>￥
							<b><c:out value="${list.tax}" /></b>
						</td>
						<td height="70" width="100" align="center" valign="top"><b>ステータス</b><br> <br>
							<b><c:out value="${list.STATUS_NAME}" /></b>
						</td>
					</tr>
					<tr bgcolor="#ffffc0">
						<td align="center"><c:out value="${list.category_name}" /></td>
						<td align="center"><c:out value="${list.item_name}" /></td>
						<td align="center"><c:out value="${list.artist}" /></td>
						<td align="center">￥ <c:out value="${list.item_price}" /></td>
						<td align="center">数量:<c:out value="${list.item_count}" /></td>
					</tr>
			</c:if>
			<c:set var="data2" value="${list}" />
		</c:forEach>
	</table>
</body>
<jsp:include page="hooter.jsp" flush="true" />
</html>