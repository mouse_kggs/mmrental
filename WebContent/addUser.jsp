<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@
	page import="java.util.ArrayList, java.util.Date, java.text.SimpleDateFormat"
 %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Bootstrap CDN -->
<link rel="stylesheet"
    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet"
    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<!-- Bootstrap CDN -->

<style type="text/css">
	<!--

			title{
			padding: 10px;
			font-weight: bold;
			align:center;
			color: #369;
			border-bottom: 3px solid #036;
			width:400px;
			}
			.box{
		     padding: 5px 10px;
		     border-radius:4px;
		     border:1px solid #222;
		     box-shadow:1px 1px 1px 1px rgba(0, 0, 0, 0.4);
		     margin: 10px auto;
		     width: 600px;
		     height:1100px;
		}	
		.textb{
		  margin-left:300px;
		  margin-right:auto;
		}　
		.t{
		  margin-right:300px;
		  margin-left:auto;
		  margin-top:3px;
		  float:left;
		}
		.e {
		 margin-left:250px;
		 margin-right:auto;
		}	
	-->
</style>

<title>MMR 新規会員登録</title>
	<jsp:include page="header1.jsp" flush="true" />
</head>
<body>


<h3 align="center">会員登録情報を入力してください</h3>
<form action="addUserConfirm" method="POST">
<div class="box">
	<div class="e"><font color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[0]}"/><c:out value="${msg[1]}"/><br></c:if></font></div>
	<div class="t">　　＊氏名</div><input class="textb" type="text" name="name" maxlength="20" value='<c:out value="${UserData.user_name}"/>'><br>
	
	<font class="e" color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[2]}"/><c:out value="${msg[3]}"/><c:out value="${msg[4]}"/><br>
	</c:if></font>
	<div class="t">　　＊メールアドレス</div><input class="textb" type="text" name="mail" maxlength="90" value='<c:out value="${UserData.mail}"/>'>

	<font class="e" color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[5]}"/><br>
	</c:if></font>
	　 <div class="t">　　メールアドレス(確認)</div><input class="textb" type="text" name="mailCheck" maxlength="90" /><br>
	<font class="e" color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[6]}"/><c:out value="${msg[7]}"/><br>
	</c:if></font>
	<div class="t">　　＊パスワード</div><input class="textb" type="text" name="password" maxlength="20" value='<c:out value="${UserData.password}"/>'>
	
	<font class="e" color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[8]}"/><br>
	</c:if></font>
	<div class="t">　　パスワード（確認）</div><input class="textb" type="text" name="passwordCheck" maxlength="20"><br>
	
	<font class="e" color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[9]}"/><c:out value="${msg[10]}"/><br>
	</c:if></font>
	<div class="t">　　＊生年月日</div><input class="textb" type="text" name="birthday" value='<c:out value="${birthday}"/>'><br>
	<font class="e" color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[11]}"/><br>
	</c:if></font>
	<div class="t">　　＊電話番号</div><input class="textb" type="text" name="tel" maxlength="11" value='<c:out value="${UserData.tel}"/>'><br>
	
	<font class="e" color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[12]}"/><br>
	</c:if></font>
　　　<div class="t">	　　＊住所 　　　    　　　　　 郵便番号</div><input class="textb" type="text" name="postal_code" maxlength="7" value='<c:out value="${UserData.postal_code}"/>'><br>
	
	<font class="e" color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[13]}"/><c:out value="${msg[14]}"/><br>
	</c:if></font>
　　　<div class="t">　　	    　　　　　　　　　　　　住所１</div><input class="textb" type="text" name="address1" maxlength="50" value='<c:out value="${UserData.address1}"/>'><br>

	<font class="e" color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[15]}"/><br>
	</c:if></font>
	<div class="t">　　　　　　　　　　　　　　住所２</div><input type="text" class="textb" name="address2" maxlength="50" value='<c:out value="${UserData.address2}"/>'><br>
	
	<font class="e" color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[16]}"/><br>
	</c:if></font>
	<div class="t">　クレジットカード情報 　　カード番号</div><input type="number" class="textb" name="card_num" maxlength="16" value='<c:out value="${UserData.card_num}"/>'><br>
	
	<font class="e" color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[17]}"/><br>
	</c:if></font>　　
　　　　<div class="t">　	　　　　　　　　カード名義人(半角ローマ字)</div><input type="text" class="textb" name="card_name" maxlength="20" value='<c:out value="${UserData.card_name}"/>'><br>
    　　
    <font class="e" color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[18]}"/><br>
	</c:if></font>
　       　<div class="t">　　　　　　　　　　　 セキュリティ番号</div><input type="number" class="textb" name="secure_code" maxlength="3" value='<c:out value="${UserData.secure_code}"/>'><br>
         
    <font class="e" color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[19]}"/><c:out value="${msg[20]}"/></c:if></font><br>   
　　　　<div  class="t">　　　　　　　　　　有効期限</div>
　　　　　　　	<select style="float:left;margin-left:300px;" name="expiredate1">　　　　    　　　　　　
　　　　　　　　　　<option value="" <c:if test="${UserData.expiredate1 == null}"><c:out value="selected"/></c:if>>年</option>
			<option value="17" <c:if test="${UserData.expiredate1 == 17}"><c:out value="selected"/></c:if>>17</option>
			<option value="18" <c:if test="${UserData.expiredate1 == 18}"><c:out value="selected"/></c:if>>18</option>
			<option value="19" <c:if test="${UserData.expiredate1 == 19}"><c:out value="selected"/></c:if>>19</option>
			<option value="20" <c:if test="${UserData.expiredate1 == 20}"><c:out value="selected"/></c:if>>20</option>
			<option value="21" <c:if test="${UserData.expiredate1 == 21}"><c:out value="selected"/></c:if>>21</option>
			<option value="22" <c:if test="${UserData.expiredate1 == 22}"><c:out value="selected"/></c:if>>22</option>
</select><select name="expiredate2">
			<option value="" <c:if test="${UserData.expiredate2 == null}"><c:out value="selected"/></c:if>>月</option>
			<option value="1" <c:if test="${UserData.expiredate2 == 1}"><c:out value="selected"/></c:if>>1</option>
			<option value="2" <c:if test="${UserData.expiredate2 == 2}"><c:out value="selected"/></c:if>>2</option>
			<option value="3" <c:if test="${UserData.expiredate2 == 3}"><c:out value="selected"/></c:if>>3</option>
			<option value="4" <c:if test="${UserData.expiredate2 == 4}"><c:out value="selected"/></c:if>>4</option>
			<option value="5" <c:if test="${UserData.expiredate2 == 5}"><c:out value="selected"/></c:if>>5</option>
			<option value="6" <c:if test="${UserData.expiredate2 == 6}"><c:out value="selected"/></c:if>>6</option>
			<option value="7" <c:if test="${UserData.expiredate2 == 7}"><c:out value="selected"/></c:if>>7</option>
			<option value="8" <c:if test="${UserData.expiredate2 == 8}"><c:out value="selected"/></c:if>>8</option>
			<option value="9" <c:if test="${UserData.expiredate2 == 9}"><c:out value="selected"/></c:if>>9</option>
			<option value="10" <c:if test="${UserData.expiredate2 == 10}"><c:out value="selected"/></c:if>>10</option>
			<option value="11" <c:if test="${UserData.expiredate2 == 11}"><c:out value="selected"/></c:if>>11</option>
			<option value="12" <c:if test="${UserData.expiredate2 == 12}"><c:out value="selected"/></c:if>>12</option>
		</select>
	<br><br><br><br>
	
<h4 style="margin-left:450px; margin-right:auto;"><input type="submit" value="登録"></h4></div></form><br>

<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- Bootstrap CDN -->
<br>
<jsp:include page="hooter.jsp" flush="true" />
</body>


</html>