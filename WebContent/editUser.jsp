<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@
	page import="java.util.ArrayList, java.util.Date, java.text.SimpleDateFormat"
 %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MMR 新規会員登録</title>
<jsp:include page="header2.jsp" flush="true" />
<style type="text/css">
	<!--
		p{margin-left:450px; margin-right:auto;}
		font{margin-left:auto; margin-right:120px;}
	-->
</style>
</head>
<body>

<h2 style="text-align:center;">会員登録情報の変更</h2>
<br> 

<form action="editUserConfirm" method="POST">
	<font color="red"><c:if test="${!empty msg}"><br>
	<c:out value="${msg[0]}"/><c:out value="${msg[1]}"/><br>
	</c:if></font>
	<p>＊氏名　　　　　　　　　　<input type="text" name="name" maxlength="20" value='<c:out value="${UserData.user_name}"/>'></p><br>
	
	<font color="red"><c:if test="${!empty msg}"><br>
	<c:out value="${msg[2]}"/><c:out value="${msg[3]}"/><c:out value="${msg[4]}"/><br>
	</c:if></font>
	<p>＊メールアドレス　　　　<input type="text" name="mail" maxlength="90" value='<c:out value="${UserData.mail}"/>'></p><br>
	
	<font color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[5]}"/><br>
	</c:if></font>
	　<p> メールアドレス(確認)　　　<input type="text" name="mailCheck" maxlength="90" value='<c:if test="${!empty oldMail}"><c:out value="${oldMail}"/></c:if><c:if test="${empty oldMail}"><c:out value="${UserData.mailCheck}"/></c:if>'></p><br>
	<font color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[6]}"/><c:out value="${msg[7]}"/><br>
	</c:if></font>
	<p>※パスワードを変更しない場合は、空欄のまま登録してください</p>
	<p>パスワード    　　　　　　　<input type="text" name="password" maxlength="20" value='<c:if test="${!empty newp}"><c:out value="${newp}"/></c:if>'></p><br>
	<font color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[8]}"/><br>
	</c:if></font>
	　<p>パスワード（確認）　　　　　<input type="text" name="passwordCheck" maxlength="20" value='<c:if test="${!empty UserData.passwordCheck}"><c:out value="${UserData.passwordCheck}"/></c:if>'></p><br>
	
	
	<font color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[9]}"/><c:out value="${msg[10]}"/><br>
	</c:if></font>
	<p>＊生年月日　　　　　　　<input type="text" name="birthday" value='<c:out value="${birthday}"/>'></p><br>
	
	<font color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[11]}"/><br>
	</c:if></font>
	<p>＊電話番号　　　　　　<input type="text" name="tel" maxlength="11" value='<c:out value="${UserData.tel}"/>'></p><br>
	
	<font color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[12]}"/><br>
	</c:if></font>
	<p>＊住所      郵便番号　　　　　<input type="text" name="postal_code" maxlength="7" value='<c:out value="${UserData.postal_code}"/>'></p><br>
	
	<font color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[13]}"/><c:out value="${msg[14]}"/><br>
	</c:if></font>
	<p>　住所１　　　　　<input type="text" name="address1" maxlength="50" value='<c:out value="${UserData.address1}"/>'></p><br>

	<font color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[15]}"/><br>
	</c:if></font>
	<p>住所２  　　　　<input type="text" name="address2" maxlength="50" value='<c:out value="${UserData.address2}"/>'></p><br>
	
	<font color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[16]}"/><br>
	</c:if></font>
	　<p>クレジットカード情報 　カード番号　　　<input type="number" name="card_num" maxlength="16" value='<c:out value="${UserData.card_num}"/>'></p><br>
	
	<font color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[17]}"/><br>
	</c:if></font>　
	　<p>カード名義人（半角ローマ字）　　　 <input type="text" name="names" maxlength="20" value='<c:out value="${UserData.card_name}"/>'></p><br>
    　　
    <font color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[18]}"/><br>
	</c:if></font>
    <p>セキュリティ番号  　　　<input type="number" name="secure_code" maxlength="3" value='<c:out value="${UserData.secure_code}"/>'></p><br>
         
    <font color="red"><c:if test="${!empty msg}">
	<c:out value="${msg[19]}"/><c:out value="${msg[20]}"/><br>
	</c:if></font>   
　　　<p> 有効期限　　　　　
		<select name="expiredate1">
		    <option value="" <c:if test="${UserData.expiredate1 == null}"><c:out value="selected"/></c:if>>年</option>
			<option value="17" <c:if test="${UserData.expiredate1 == 17}"><c:out value="selected"/></c:if>>17</option>
			<option value="18" <c:if test="${UserData.expiredate1 == 18}"><c:out value="selected"/></c:if>>18</option>
			<option value="19" <c:if test="${UserData.expiredate1 == 19}"><c:out value="selected"/></c:if>>19</option>
			<option value="20" <c:if test="${UserData.expiredate1 == 20}"><c:out value="selected"/></c:if>>20</option>
			<option value="21" <c:if test="${UserData.expiredate1 == 21}"><c:out value="selected"/></c:if>>21</option>
			<option value="22" <c:if test="${UserData.expiredate1 == 22}"><c:out value="selected"/></c:if>>22</option>
		</select>　		
		<select name="expiredate2">
			<option value="" <c:if test="${UserData.expiredate2 == null}"><c:out value="selected"/></c:if>>月</option>
			<option value="1" <c:if test="${UserData.expiredate2 == 1}"><c:out value="selected"/></c:if>>1</option>
			<option value="2" <c:if test="${UserData.expiredate2 == 2}"><c:out value="selected"/></c:if>>2</option>
			<option value="3" <c:if test="${UserData.expiredate2 == 3}"><c:out value="selected"/></c:if>>3</option>
			<option value="4" <c:if test="${UserData.expiredate2 == 4}"><c:out value="selected"/></c:if>>4</option>
			<option value="5" <c:if test="${UserData.expiredate2 == 5}"><c:out value="selected"/></c:if>>5</option>
			<option value="6" <c:if test="${UserData.expiredate2 == 6}"><c:out value="selected"/></c:if>>6</option>
			<option value="7" <c:if test="${UserData.expiredate2 == 7}"><c:out value="selected"/></c:if>>7</option>
			<option value="8" <c:if test="${UserData.expiredate2 == 8}"><c:out value="selected"/></c:if>>8</option>
			<option value="9" <c:if test="${UserData.expiredate2 == 9}"><c:out value="selected"/></c:if>>9</option>
			<option value="10" <c:if test="${UserData.expiredate2 == 10}"><c:out value="selected"/></c:if>>10</option>
			<option value="11" <c:if test="${UserData.expiredate2 == 11}"><c:out value="selected"/></c:if>>11</option>
			<option value="12" <c:if test="${UserData.expiredate2 == 12}"><c:out value="selected"/></c:if>>12</option>
		</select></p>
		
		<br><br>
	<input style="margin-left:800px; margin-right:auto;" type="submit" value="登録">
</form>
	


</body>

<jsp:include page="hooter.jsp" flush="true" />

</html>