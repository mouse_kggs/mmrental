<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@
	page import="java.util.ArrayList, test.ItemDTO"
 %>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Bootstrap CDN -->
<link rel="stylesheet"
    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet"
    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<!-- Bootstrap CDN -->


<style type="text/css">
	<!--
      		.title　{
 				　　　text-shadow: text-shadow: 2px 4px 3px rgba(0,0,0,0.3);
 				    margin-left:40px;
 				    margin-right:auto;
 				    width:100px;   
			}
      		.right  {
                   margin-left:auto;           
                   margin-right:50px;
                   width:500px;     
      		}
      		.msg  {
                   margin-left:auto;           
                   margin-right:50px;
                   width:250px;     
      		}  	
      		.new  {
                   margin-left:auto;           
                   margin-right:100px;
                   width:500px;     
      		}
      		.gazo  {
                   margin-left:160px;          
                   margin-right:auto;
                   float:left;
      		}
      		.new  {
                   margin-left:auto;           
                   margin-right:90px;
                   width:500px;     
      		}
      		.title  {
        		   margin-left:30px;           
                   margin-right:auto;
                   float:left;
                   width:30%;
             }
             .detail  {
        		  margin-left:auto;           
                  margin-right:20px;
                  width:40%;
             }
             .under {
  				border-bottom: dashed 2px orange;
  			}
            
            h1 {
				padding: .25em 0 .25em .75em;
				border-left: 6px solid #ccc;
			}
      		
	-->
</style>
<head>
<title>MMRレンタル　- カート内確認</title>
</head>
<div id="header">
<c:if test="${empty userProfile}">
	<jsp:include page="header1.jsp" flush="true" />
</c:if>
<c:if test="${!empty userProfile}">
	<jsp:include page="header2.jsp" flush="true" />
</c:if>
</div>
<%
	session.setAttribute("backTo", "showCart");
%>
<body>
<br> 
<br>
<h3 class="h1" style="margin-left:150px; margin-right:auto;">　　カート内の商品</h3>
<hr class="under">
<h3 class="new">新旧　　　カテゴリ　　　価格　　　数量</h3>
<hr class="under">
<br>
<form action="deliveryConfirm" method="POST">
<c:forEach var="item" items="${cartItemList}">
	<div class="gazo"><a href='itemDetail?item_id=<c:out value="${item.item_id}"/>'><c:choose><c:when test="${item.category_name == 'DVD'}"><img style="width: 150px; height: 200px" src="image?id=${item.item_id}"></c:when>
	<c:when test="${item.category_name == 'CD'}"><img style="width: 170px; height: 170px" src="image?id=${item.item_id}"></c:when>
	<c:otherwise><img style="width: 150px; height: 200px" src="image?id=${item.item_id}"></c:otherwise></c:choose></a></div><br><br><br>
<h3 class="title"><a href='itemDetail?item_id=<c:out value="${item.item_id}"/>'><c:out value="${item.item_name}"/></a><br>
/ <c:out value="${item.artist}"/></h3>　 　
<h3 class="detail"><c:out value="${item.new_and_old_name}"/>　　  <c:out value="${item.category_name}"/>　　￥<c:out value="${item.price}"/>
 		　　<select name="amount">			           
		   			<option value="1" <c:if test="${item.amount == 1}"><c:out value="selected"/></c:if>>1</option>
		   			<option value="2" <c:if test="${item.amount == 2}"><c:out value="selected"/></c:if>>2</option>
		   			<option value="3" <c:if test="${item.amount == 3}"><c:out value="selected"/></c:if>>3</option>
		   			<option value="4" <c:if test="${item.amount == 4}"><c:out value="selected"/></c:if>>4</option>
		   			<option value="5" <c:if test="${item.amount == 5}"><c:out value="selected"/></c:if>>5</option>
		   			<option value="6" <c:if test="${item.amount == 6}"><c:out value="selected"/></c:if>>6</option>
		   			<option value="7" <c:if test="${item.amount == 7}"><c:out value="selected"/></c:if>>7</option>
		   			<option value="8" <c:if test="${item.amount == 8}"><c:out value="selected"/></c:if>>8</option>
					<option value="9" <c:if test="${item.amount == 9}"><c:out value="selected"/></c:if>>9</option>
					<option value="10" <c:if test="${item.amount == 10}"><c:out value="selected"/></c:if>>10</option>
					</select>
					　<a href='deleteCart?item_id=<c:out value="${item.item_id}"/>'>削除</a></h3><br><br><br>
<hr class="under">
</c:forEach>
<div class="msg"><c:if test="${empty userProfile}"><div style="color:red"><c:out value="会員登録またはログインが必要です"/></div></c:if></div>
<div class="right"><h2>小計（ <c:out value="${TotalAmount}"/> ）点 ￥<c:out value="${TotalPrice}"/>
　<input type="submit" value="レンタル" class="btn btn-primary btn-lg" <c:choose><c:when test="${empty itemList}"><c:out value="disabled"/></c:when><c:when test="${empty userProfile}"><c:out value="disabled"/></c:when></c:choose>></h2></div>
</form>
</body>
<jsp:include page="hooter.jsp" flush="true" />
</html>