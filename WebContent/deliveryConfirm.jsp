<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@
	page import="java.util.ArrayList, java.util.Date, java.text.SimpleDateFormat"
 %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>MMR お届け日時確認</title>
<script type="text/javascript">
	<!--　
	function check(elem){
	  	var item = document.getElementsByName("pchoise")
	  	for(var i=0; i<item.length; i++){
	  		if(elem.value == "credit_point"){
	  		  item[0].checked = true;
   		  	  item[i].removeAttribute("disabled");
			}else{
				item[i].checked = false;
				item[i].setAttribute("disabled");
			}
		}
	}
	<!--　
	function pcheck(elm){
	  	var text = document.getElementsByName("textPoint")
	  		if(elm.value == "some"){
	  		  text[0].removeAttribute("disabled");
			}else{
				text[0].setAttribute("disabled");
			}
		
	}
	
	// -->
</script>
<style type="text/css">
<!--
		.box{
		     padding: 30px 20px;
		     border-radius:4px;
		     border:1px solid #222;
		     box-shadow:1px 1px 1px 1px rgba(0, 0, 0, 0.4);
		     margin: 20px auto;
		     width: 480px;
		     height:500px;
		}	
		.div1{
			margin-right:20px;
			margin-left:auto;
		}
		.right{
			margin-top:10;
			margin-right:auto;
			margin-left:200px;
			width: 450px;
		}
		.right1{
			margin-right:auto;
			margin-left:230px;
			width: 300px;
		}
		
	-->
</style>

</head>
<body> 
<br><br>
<div style="colot:red; text-align:center"><c:out value="${success}"/></div>
<h2 align="center">お届け日時 / 支払い方法の確認</h2>
<div class="box">
<form action="rentalConfirm" method="POST">
<h3>お届け日:　<select name="delivery_date">
			<option value="1" <c:if test="${date == 1}"><c:out value="selected"/></c:if>><c:out value="${dates.get(0)}"/></option>
			<option value="2" <c:if test="${date == 2}"><c:out value="selected"/></c:if>><c:out value="${dates.get(1)}"/></option>
			<option value="3" <c:if test="${date == 3}"><c:out value="selected"/></c:if>><c:out value="${dates.get(2)}"/></option>
			<option value="4" <c:if test="${date == 4}"><c:out value="selected"/></c:if>><c:out value="${dates.get(3)}"/></option>
			<option value="5" <c:if test="${date == 5}"><c:out value="selected"/></c:if>><c:out value="${dates.get(4)}"/></option>
			<option value="6" <c:if test="${date == 6}"><c:out value="selected"/></c:if>><c:out value="${dates.get(5)}"/></option>
			<option value="7" <c:if test="${date == 7}"><c:out value="selected"/></c:if>><c:out value="${dates.get(6)}"/></option>
		</select> 
　時間帯 :　<select name="delivery_time">
			<option value="1" <c:if test="${time == 1}"><c:out value="selected"/></c:if>>午前中</option>
			<option value="2" <c:if test="${time == 2}"><c:out value="selected"/></c:if>>12:00 ~ 15:00</option>
			<option value="3" <c:if test="${time == 3}"><c:out value="selected"/></c:if>>15:00 ~ 18:00</option>
			<option value="4" <c:if test="${time == 4}"><c:out value="selected"/></c:if>>18:00 ~ 21:00</option>
		</select><br></h3>
<h4 class="right"><font color="red"><c:if test="${!empty error}"><c:out value="${error[0]}"/><a href="editUser"><c:out value="${error[1]}"/></a><c:out value="${error[2]}"/><c:out value="${error[3]}"/></c:if></font></h4>
<h3 style=float:left;>  支払い方法</h3>
		  <h3 class="right"><input type="radio" name="payment" onclick="check(this)" value="credit" <c:if test="${choose == 'credit'}"><c:out value="checked"/></c:if>>クレジット <br></h3>
		  <h3 class="right"><input type="radio" name="payment" onclick="check(this)" value="point" <c:if test="${choose == 'point'}"><c:out value="checked"/></c:if>>ポイント<br></h3>
		  <h3 class="right"><input type="radio" name="payment" onclick="check(this)" value="credit_point" <c:if test="${choose == 'credit_point'}"><c:out value="checked"/></c:if>>クレジットとポイントの併用</h3>
		  <h3 class="right1"><input type="radio" name="pchoise" value="all" disabled>ポイントをすべて使う</h3>
		  <h3 class="right1"><input type="radio" name="pchoise" value="some" onclick="pcheck(this)" disabled>ポイントを一部使う<br>
		  　　<input type="number" name="textPoint" style="width:80px;" disabled>　ポイント</h3>
<h3 style=float:left;>　　現在 <c:if test="${!empty userProfile && userProfile.point != null}"><c:out value="${userProfile.point}"/></c:if> ポイント <br>
>><a href="pointCharge">ポイントをチャージする</a></h3>　　　
<h2>　計<c:out value="${TotalAmount}"/>点　￥<c:out value="${TotalPrice}"/></h2><h2 style="text-align:center;">　　　　　　
　　　　　　　　<input type="submit" value="確定"></h2><br><br>　
</form></div>
</body>
<jsp:include page="hooter.jsp" flush="true" />
</html>