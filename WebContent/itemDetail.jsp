<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@
	page import="test.ItemDTO"
 %>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Bootstrap CDN -->
<link rel="stylesheet"
    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet"
    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<!-- Bootstrap CDN -->

<head>
<title>MMRレンタル　- 商品詳細</title>
</head>
<div id="header">
<c:if test="${empty userProfile}">
	<jsp:include page="header1.jsp" flush="true" />
</c:if>
<c:if test="${!empty userProfile}">
	<jsp:include page="header2.jsp" flush="true" />
</c:if>
</div>

<style type="text/css">
	<!--
						 width:500px;
				margin:100 auto;
				}
				/* ----- ヘッダー ----- */
				#header {
				height:150px;
				background-color:#4169e1;
				}
				/* ----- 右サイド ----- */
				#right {
				width:350px;
				min-height:400px;
				height:600px ;
				background-color:#d6eaff;
				float:right;
				margin-top:20px;
				margin-right:60px;
				text-align:center;
				padding-left:10px;
				padding-right:10px;
				}
				
				/* ----- メイン ----- */
				#content { 
				width:790px;
				min-height:400px;
				height:600px;
				background-color:#d6eaff;
				margin-top:10px;
				margin-left:60px;
				}
				#content2 { 
				width:800px;
				min-height:400px;
				height:560px;
				background-color:#d6eaff;
				margin-top:10px;
				margin-left:auto;
				margin-right:auto;
				
				}
				.white { 
					padding-top:15px;
					padding-left:5px;
					text-align:center;
					padding-right:5px;
				}
				.white1 { 
					padding-left:5px;
					text-align:center;
					padding-right:5px;
				}
				.title { 
					padding-top:15px;
					text-shadow: 5px 5px 1px #999999;
					font-weight:bold;
				}
				hr1 {
					border-top: 2px dashed #8c8b8b;
				}	
			
				#button1 {
       				margin-top:5px;
      			}
      			.square_btn{
				    position: relative;
				    display: inline-block;
				    font-weight: bold;
				    padding: 0.25em 0.5em;
				    text-decoration: none;
				    color: #00BCD4;
				    background: #ECECEC;
				    transition: .4s;
				  }
				
				.square_btn:hover {
				    background: #00bcd4;
				    color: white;
				}
      			
      			
	-->
</style>

<body>
<c:set scope="session" var="returnTo" value="${'itemDetail?item_id=' += detail.item_id }"></c:set>
<c:if test="${!empty userProfile}"><div id="right">
<h3 style="padding-top:5px;">簡単！　ワンクリック注文</h3>
<hr class="hr1">
<form action="oneClick?item_id=<c:out value="${detail.item_id}"/>&price=<c:out value="${detail.price}"/>" method="POST">
お届け日 :<select name="delivery_date">
			<option value="1" <c:if test="${date == 1}"><c:out value="selected"/></c:if>><c:out value="${dates.get(0)}"/></option>
			<option value="2" <c:if test="${date == 2}"><c:out value="selected"/></c:if>><c:out value="${dates.get(1)}"/></option>
			<option value="3" <c:if test="${date == 3}"><c:out value="selected"/></c:if>><c:out value="${dates.get(2)}"/></option>
			<option value="4" <c:if test="${date == 4}"><c:out value="selected"/></c:if>><c:out value="${dates.get(3)}"/></option>
			<option value="5" <c:if test="${date == 5}"><c:out value="selected"/></c:if>><c:out value="${dates.get(4)}"/></option>
			<option value="6" <c:if test="${date == 6}"><c:out value="selected"/></c:if>><c:out value="${dates.get(5)}"/></option>
			<option value="7" <c:if test="${date == 7}"><c:out value="selected"/></c:if>><c:out value="${dates.get(6)}"/></option>
		    </select><br>
時間帯    :<select name="delivery_time"> 
			<option value="1" <c:if test="${time == 1}"><c:out value="selected"/></c:if>>午前中</option>
			<option value="2" <c:if test="${time == 2}"><c:out value="selected"/></c:if>>12:00 ~ 15:00</option>
			<option value="3" <c:if test="${time == 3}"><c:out value="selected"/></c:if>>15:00 ~ 18:00</option>
			<option value="4" <c:if test="${time == 4}"><c:out value="selected"/></c:if>>18:00 ~ 21:00</option>
		</select>		
<div id="button">
<h5><font color="red"><c:if test="${!empty error}"><c:out value="${error[0]}"/><a href="editUser"></a></c:if></font><br></h5>
<input type="submit" name="credit" value="クレジットでレンタル" class="btn btn-primary">
</div>
<hr class="hr1">
<h5><font color="red"><c:if test="${!empty error}"><c:out value="${error[1]}"/></c:if></font><br></h5>
<div id="button"><input type="submit" name="point" value="ポイントでレンタル" class="btn btn-primary">
</div>
<hr class="hr1">
<font color="red"><c:if test="${!empty error}"><c:out value="${error[2]}"/><c:out value="${error[3]}"/><br></c:if></font>
<h5 style="float:left;">現在 <c:if test="${!empty userProfile && userProfile.point != null}"><c:out value="${userProfile.point}"/></c:if>
<c:if test="${empty userProfile || userProfile.point == null}">0</c:if>ポイント<br>　>> <a href="pointCharge">ポイントチャージ</a></h5>
<input type="radio" name="pchoise" value="all" checked <c:if test="${p == 'all'}"><c:out value="checked"/></c:if>>ポイントをすべて使う<br>
<input type="radio" name="pchoise" value="some" <c:if test="${p == 'some'}"><c:out value="checked"/></c:if>>ポイントを一部使う<br>
<input type="number" name="textPoint" style="width:80px; margin-left:182px; margin-right:auto;">ポイント
<div id="button1">
<input type="submit" name="credit_point" value="クレジット＆ポイントでレンタル" class="btn btn-primary">
</div></form>
<hr class="white">
<h3>小計 (1点)　　￥<c:out value="${detail.price}" /></h3>
</div>


<div id="content">
<h2 class="title" style="padding-top:30px;padding-left:100px;"> <c:out value="${detail.item_name}"/> </h2>
	<div style="float:left;  margin-left: 50px; padding-top:5px;"><img style="width: 350px; height: 450px" src="image?id=${detail.item_id}"></div>
<br>
<h3 class="white">　アーティスト /</h3>
<h3 class="white1"> <c:out value="${detail.artist}"/></h3>
<h3 class="white">　カテゴリ / <c:out value="${detail.category_name}"/></h3>
<h3 class="white">　ジャンル / <c:out value="${detail.genre_name}"/></h3>
<h3 class="white">　値段 / ￥<c:out value="${detail.price}"/></h3>
<c:if test="${!empty detail.remarks}"><h3 class="white">　その他 / <c:out value="${detail.remarks}"/></h3></c:if><br><br>

<%-- エラーメッセージがあれば表示 --%>
<% if(null != request.getAttribute("CartError")){ %>
<div style="color:red"><%= request.getAttribute("CartError") %></div>
<% } %><br><div style="display:inline-flex">
<form style="margin:1px; padding:1px; color:#ffa500" action='<c:out value="${backTo}"/>' method="POST">　
　　　　　<input type="submit" value="戻る" class="btn btn-default"></form>
　　<form class="orange" action='addCart?item_id=<c:out value="${detail.item_id}"/>' method="POST">     
　　　<input type="submit" value="カートに追加" class="square_btn"></form>　　　
</div>
</div>
</c:if>


<c:if test="${empty userProfile}">
<div id="content2">
<h2 class="title" style="padding-top:30px;padding-left:50px;">「 <c:out value="${detail.item_name}"/> 」</h2>
	<div style="float:left;  margin-left: 30px; padding-top:10px;"><img style="width: 350px; height: 450px" src="image?id=${detail.item_id}"></div>
<br>
<h3 class="white">　アーティスト /</h3>
<h3 class="white1"> <c:out value="${detail.artist}"/></h3>
<h3 class="white">　カテゴリ / <c:out value="${detail.category_name}"/></h3>
<h3 class="white">　ジャンル / <c:out value="${detail.genre_name}"/></h3>
<h3 class="white">　値段 / ￥<c:out value="${detail.price}"/></h3>
<c:if test="${!empty detail.remarks}"><h3 class="white">　その他 / <c:out value="${detail.remarks}"/></h3></c:if><br><br>

<%-- エラーメッセージがあれば表示 --%>
<% if(null != request.getAttribute("CartError")){ %>
<div style="color:red; text-align:center;"><%= request.getAttribute("CartError") %></div>
<% } %>　
<div style="display:inline-flex">　
	<form style="margin:1px; padding:1px; color:#ffa500" action='<c:out value="${backTo}"/>' method="POST">
　　	 　<input type="submit" value="戻る" class="btn btn-default"></form><br>
　　　　<form class="orange" action='addCart?item_id=<c:out value="${detail.item_id}"/>' method="POST">   
　　　　<input type="submit" value="カートに追加" class="square_btn"></form>　　　
</div>

</div>

</c:if>

<div id="footer">
<br>
</div>
</body>
</html>