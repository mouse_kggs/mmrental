<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MMR オンライン</title>
</head>
<body>

<div style="height:110px; background-color:#ddddff">
	<a href="homeView"><img src="img/MMR-logo-1.png" style="width:133px; height:100px; float:left"></a>
	<div style="text-align:right">
		<div style="height:100px;float:right; padding-top:5px; padding-right:5px">
			<!-- カート＋ログアウト -->
			<div style="float:right">
				<div style="height:60px">
					<c:if test="${!empty itemList}">
						<a href="showCart"><img width="70px" height="60px" src="img/cart.png"></a>
					</c:if>
				</div>
				<form action="logout" method="get"><input style="margin-top:10px" class="btn btn-primary btn-sm" type="submit" value="ログアウト"></form>
			</div>
		</div>
		<div style="height:100px;float:right; padding-top:15px;margin-right:10px;">
			<div style="height:35px; border-bottom:solid 2px">
				<h4>ようこそ<c:out value="${userProfile.user_name}"/>さん!  　現在<c:out value="${userProfile.point}"/>ポイント</h4>
			</div><div style="height:50px; padding-top:5px; font-size:medium">
				<a href='editUser'>＞会員登録情報の変更</a>　
				<a href='pointCharge'>＞ポイントチャージ</a>
			</div>
		</div>
	</div>
</div>
</body>
</html>