<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@
	page import="dto.GenreDTO, java.util.ArrayList, dao.GenreDAO"
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/searchpage.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<%
	request.setAttribute("categoryList", GenreDAO.selectCategoryAllStatic());
	request.setAttribute("genreList", GenreDAO.selectGenreAllStatic());
%>
<c:choose>
	<c:when test="${empty userProfile}">
		<jsp:include page="header1.jsp" flush="true" />
	</c:when>
	<c:otherwise>
		<jsp:include page="header2.jsp" flush="true" />
	</c:otherwise>
</c:choose>
<div style="height:32px; background-color:#ddddff; padding:2px; border-top:solid 1px; border-bottom:solid 1px">
	<div style="width:70%; float:left">
		<form action="searchItem" method="post" name="test">
			<div style="width:12%; float:left; font-weight:bold">商品検索</div>
			<select style="width:15%; height:90%; padding:1px; margin:0px" id="categorySelectBox" class="categoryBox" name="category" onChange="categoryChanged()">
				<option value="0" <c:if test="${searchCategory == 0}"><c:out value="selected"/></c:if>>カテゴリ</option>
				<c:forEach var="category" items="${categoryList}">
					<option value='<c:out value="${category.category_id}"/>' <c:if test="${searchCategory == category.category_id}"><c:out value="selected"/></c:if>><c:out value="${category.category_name}"/></option>
				</c:forEach>
			</select>
			<select style="width:25%; height:90%; padding:1px; margin:0px" id="genreSelectBox" class="genreBox" name="genre">
				<option value="0" >ジャンル</option>
				<c:forEach var="genre" items="${genreList}">
					<option class='<c:out value="${genre.category_id}"/>' value='<c:out value="${genre.genre_id}"/>' <c:if test="${searchGenre == genre.genre_id}"><c:out value="selected"/></c:if>><c:out value="${genre.genre_name}"/></option>
				</c:forEach>
			</select>
			<input style="width:35%; height:90%; padding:0px; margin:0px" type="text" name="searchString" size="25" maxlength="100" value='<c:out value="${searchString}"/>'>
			<input style="width:10%; height:90%; padding:0px; margin:0px" type="submit" value="検索">
		</form>
	</div>
	<div style="float:right; width:10%"><form action="searchItem?category=3" method="post"><input class="btn-primary" style="background-color:#ddddff; color:black; width:100%" type="submit" value="Blu-ray"/></form></div>
	<div style="float:right; width:10%"><form action="searchItem?category=1" method="post"><input class="btn-primary" style="background-color:#ddddff; color:black; width:100%" type="submit" value="CD"/></form></div>
	<div style="float:right; width:10%"><form action="searchItem?category=2" method="post"><input class="btn-primary" style="background-color:#ddddff; color:black; width:100%" type="submit" value="DVD"/></form></div>
	
</div>

<br>
</body>
</html>