<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Bootstrap CDN -->
<link rel="stylesheet"
    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet"
    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<!-- Bootstrap CDN -->


<title>MMR レンタルサイト</title>
</head>
<script type="text/javascript">
	<!--
	function login(){
		if(fm.mail.value == "" || fm.password.value == ""){
			alert("メールアドレスとパスワードを入力してください");
			return false;
		}
		return true;
	}
	// -->
</script>
<body>
<div style="height:110px; background-color:#ddddff">
	<a href="homeView"><img src="img/MMR-logo-1.png" style="width:133px; height:100px; float:left"></a>
	<div style="text-align:right">
		<div style="height:100px;float:right; padding-top:5px; padding-right:5px">
			<!-- ログインフォーム -->
			<form name="fm" action="login" method="post">
				<div style="float:left; margin-right:5px; padding-top:1px">
					<h5 style="text-align:center; font-weight:bold;">会員の方</h5>
					メールアドレス:<input type="text" name="mail"><br>
					パスワード:<input type="password" name="password"><br>
					<%-- エラーメッセージがあれば表示 --%>
					<% if(null != request.getAttribute("Error")){ %>
						<div style="color:red"><%= request.getAttribute("Error") %></div>
					<% } %>
				</div>
				<div style="float:left">
					<div style="height:60px">
						<c:if test="${!empty itemList}">
							<a href="showCart"><img width="70px" height="60px" src="img/cart.png"></a>
						</c:if>
					</div>
					<input style="margin-top:10px" class="btn btn-primary btn-sm" type="submit" value="ログイン" onclick="return login()">
				</div>
			</form>
			
		</div>
		<div style="width:140px; height:100px;float:right; padding-top:5px;margin-right:10px;">
			<div style="padding-right:10px;padding-top:11px;height:100px; border-right:solid 2px">
				<h5 style="text-align:right; font-weight:bold;">初めての方</h5>
				<form name="new" action="addUser">
					<input type="submit" value="新規会員登録" class="btn btn-primary btn-sm">
				</form>
			</div>
		</div>
	</div>
</div>
</body>
</html>