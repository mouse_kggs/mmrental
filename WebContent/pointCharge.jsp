<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@
	page import="java.util.ArrayList, test.ItemDTO"
 %>
<!DOCTYPE html> 
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Bootstrap CDN -->
<link rel="stylesheet"
    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet"
    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<!-- Bootstrap CDN -->

<head>
<title>MMRレンタル　- カート内確認</title>

<style type="text/css">
<!--
		.div1{
			margin-right:20px;
			margin-left:auto;
		}
		.right{
			margin-top:10px;
			margin-right:auto;
			margin-left:700px;
			width: 300px;
		}
		.right1{
			margin-left:660px;
			margin-right:auto;
		}
		
		.stripe{ 
  			position: relative;
  			padding: 0.3em;
		}
		.stripe:after {
		  	content: '';
		  	position: absolute;
		  	left: 0;
		  	bottom: 0;
		  	width: 100%;
		  	height: 7px;
		  	background: repeating-linear-gradient(-45deg, skyblue, skyblue 2px, white 2px, white 4px);
		}
		
	-->
</style>

</head>
<div id="header">
	<jsp:include page="header2.jsp" flush="true" />
</div>
<body>
<br>
<br>
<h3 style="margin-left:260px; margin-right:auto;">ポイントをチャージする</h3>
<hr>
<br>
<h2 class="stripe" style="float:left; margin-left:260px;margin-right:150px;">現在 <c:out value="${userProfile.getPoint()}" /> ポイント</h2>
<h2>＊チャージする金額<br></h2>
<h4 style="margin-left:300px;margin-right:150px;float:left;"><c:if test="${!empty success}">
	<% out.println((String)request.getAttribute("success")); %></c:if></h4>
<h5><font color="red">
<c:if test="${!empty Error}">
	<% out.println((String)request.getAttribute("Error")); %>
</c:if></font></h5> 
<form name="webmoney_code" action="pointRegist" method="POST">
<h3 class="right"><input type="radio" name="charge" value="500" <c:if test="${selected == '500'}"><c:out value="checked"/></c:if> checked>500円</h3>
<h3 class="right"><input type="radio" name="charge" value="1000" <c:if test="${selected == '1000'}"><c:out value="checked"/></c:if>>1,000円</h3>
<h3 class="right"><input type="radio" name="charge" value="2000" <c:if test="${selected == '2000'}"><c:out value="checked"/></c:if>>2,000円</h3>
<h3 class="right"><input type="radio" name="charge" value="3000" <c:if test="${selected == '3000'}"><c:out value="checked"/></c:if>>3,000円</h3>
<h3 class="right"><input type="radio" name="charge" value="5000" <c:if test="${selected == '5000'}"><c:out value="checked"/></c:if>>5,000円</h3>
<h3 class="right"><input type="radio" name="charge" value="10000" <c:if test="${selected == '10000'}"><c:out value="checked"/></c:if>>10,000円</h3>
<h2 class="right1">＊Webマネーコード</h2>
<h4 class="right1"><font color="red">
<c:if test="${!empty chargeError}">
　　　<% out.println((String)request.getAttribute("chargeError")); %>
</c:if></font></h4>
<h4 class="right1">　　<input maxlength="16" type="text" name="code" width="120px;"><br>　　　　　　　　　　　　　
<input style="margin-top:10px;margin-left:auto;margin-right:50px" type="submit" value="送信"></h4></form><br><br>
</body>
<jsp:include page="hooter.jsp" flush="true" />
</html>