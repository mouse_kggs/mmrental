package historyDAO;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DataSourceManager {

	public static Connection getConnections() throws SQLException, NamingException {

		try {
			// Serverの Server.xmlの中にデータソースを定義した
			Context ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/mysql/mmr");
			System.out.println("ok");
			return ds.getConnection();

		} catch (SQLException e) {
			// ★エラーページに遷移
			throw e;
		}
	}
}
