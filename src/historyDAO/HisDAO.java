package historyDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import historyDTO.HisDTO;

public class HisDAO extends DataSourceManager {

	protected Connection con;

	public HisDAO(Connection con) {
		this.con = con;
	}
	/*
	 * 購入履歴データ取得
	 */
	public ArrayList<HisDTO> hissearch(int user_id) throws SQLException {

		// SQL文作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(rental.RENTAL_NUMBER),");
		sb.append("        rental.ORDER_DATETIME");
		sb.append("        ,rental.RETURN_DATETIME");
		sb.append("        ,rental.RENTAL_NUMBER");
		sb.append("        ,COUNT(*) AS '数量'");
		sb.append("        ,STATUS_NAME");
		sb.append("        ,category.CATEGORY_NAME");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,item.ARTIST");
		sb.append("        ,rental_detail.PRICE");
		sb.append("        ,COUNT(rental_detail.rental_detail_number) AS '個数'");
		sb.append("        ,COUNT(*) AS '数量'");
		sb.append("    FROM");
		sb.append("        (");
		sb.append("            (");
		sb.append("                (");
		sb.append("                    (");
		sb.append("                        (");
		sb.append("                            (");
		sb.append("                                USER INNER JOIN rental");
		sb.append("                                    ON user.USER_ID = rental.USER_ID");
		sb.append("                            ) INNER JOIN status");
		sb.append("                            ON rental.STATUS_ID = status.STATUS_ID");
		sb.append("                        ) INNER JOIN rental_detail");
		sb.append("                        ON rental.RENTAL_NUMBER = rental_detail.RENTAL_NUMBER");
		sb.append("                    )");
		sb.append("                    INNER JOIN item");
		sb.append("                    ON rental_detail.ITEM_ID = item.ITEM_ID");
		sb.append("                ) INNER JOIN genre");
		sb.append("                ON item.GENRE_ID = genre.GENRE_ID");
		sb.append("            ) INNER JOIN category");
		sb.append("            ON genre.CATEGORY_ID = category.CATEGORY_ID");
		sb.append("        )");
		sb.append(" WHERE");
		sb.append(" user.USER_ID = ?");
		sb.append(" GROUP BY");
		sb.append(" item.ITEM_ID");
		sb.append(",rental.RENTAL_NUMBER");
		sb.append(" ORDER BY");
		sb.append(" rental_number DESC");

		/*
		 * 見出しと商品枠を判別
		 */
		ArrayList<HisDTO> list = new ArrayList<HisDTO>();
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			ps.setInt(1, user_id);
			ResultSet rs = ps.executeQuery();

			//totalListにuser_idを代入
			ArrayList<HisDTO> totalList = getTotal(user_id);
			int index = 0;
			HisDTO rs2 = null;

			int num1;
			int num2 = 0;

			//table項目の値を取得
			while (rs.next()) {
				HisDTO hisdata = new HisDTO();
				hisdata.setORDER_DATETIME(rs.getTimestamp("ORDER_DATETIME"));
				hisdata.setRETURN_DATETIME(rs.getDate("RETURN_DATETIME"));
				hisdata.setRENTAL_NUMBER(rs.getInt("rENTAL_NUMBER"));
				hisdata.setSTATUS_NAME(rs.getString("STATUS_NAME"));
				hisdata.setCategory_name(rs.getString("CATEGORY_NAME"));
				hisdata.setItem_name(rs.getString("ITEM_NAME"));
				hisdata.setArtist(rs.getString("item.ARTIST"));
				hisdata.setItem_price(rs.getInt("rental_detail.PRICE"));
				hisdata.setItem_count(rs.getInt("数量"));
				hisdata.setMax(rs.getInt("個数"));

				//num1にレンタル番号を代入
				num1 = hisdata.getRENTAL_NUMBER();

				//num1とnum2が等しくなかったら次の配列番号に移動
				if (num1 != num2) {
					System.out.println(num1 + " " + num2);
					rs2 = totalList.get(index++);
				}

				//num2にnum1を代入
				num2 = num1;
				hisdata.setMax(rs2.getMax());
				hisdata.setTax(rs2.getTax());

				list.add(hisdata);
			}

			//次の行に移動
			if (rs.next()) {
				return list;
			} else {
				return list;
			}
		} catch (SQLException e) {
			//提出時外す
			e.printStackTrace();
			throw e;
		}
	}

	/*
	 * 数量と合計金額を求める
	 */
	private ArrayList<HisDTO> getTotal(int user_id) throws SQLException {

		StringBuffer sb = new StringBuffer();
		sb.append("select ");
		sb.append("rental.RENTAL_NUMBER,");
		sb.append("count(*) as '数量',");
		sb.append("SUM(rental_detail.PRICE) as '合計金額'");
		sb.append("FROM ((((((user INNER JOIN rental ON ");
		sb.append("user.USER_ID = rental.USER_ID)");
		sb.append("INNER JOIN status ON ");
		sb.append("rental.STATUS_ID = status.STATUS_ID)");
		sb.append("INNER JOIN rental_detail ON ");
		sb.append("rental.RENTAL_NUMBER = rental_detail.RENTAL_NUMBER)");
		sb.append("INNER JOIN item ON ");
		sb.append("rental_detail.ITEM_ID = item.ITEM_ID)");
		sb.append("INNER JOIN genre ON ");
		sb.append("item.GENRE_ID = genre.GENRE_ID)");
		sb.append("INNER JOIN category ON ");
		sb.append("genre.CATEGORY_ID = category.CATEGORY_ID)");
		sb.append("where user.USER_ID = ?");
		sb.append(" group by rental.RENTAL_NUMBER");
		sb.append(" ORDER BY rental_number DESC");

		/*
		 * 数量と合計金額を求めるlist
		 */
		ArrayList<HisDTO> list = new ArrayList<HisDTO>();
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			ps.setInt(1, user_id);
			ResultSet rs = ps.executeQuery();

			//次の行にカーソル移動
			while (rs.next()) {
				HisDTO hisdata = new HisDTO();
				hisdata.setRENTAL_NUMBER(rs.getInt("RENTAL_NUMBER"));
				hisdata.setMax(rs.getInt("数量"));
				hisdata.setTax(rs.getInt("合計金額"));
				list.add(hisdata);
			}
			return list;
		} catch (SQLException e) {
			//提出時外す
			e.printStackTrace();
			throw e;
		}
	}
}