package historyDTO;

import java.sql.Date;
import java.sql.Timestamp;

public class HisDTO {

	private int item_id;				//商品ID
	private String item_name;			//商品名
	private String category_name;		//カテゴリ名
	private String genre_name;			//ジャンル名
	private String recommended_flg;		//削除フラグ
	private String new_and_old_name;	//新旧区分
	private String image;				//画像
	private String artist;				//アーティスト
	private int item_price;				//単価
	private String remarks;				//その他
	private Date RETURN_DATETIME;		//返却日
	//private String status_name;			//ステータス ダブり*****
	private int RENTAL_NUMBER;			//貸出番号
	private String STATUS_NAME;			//ステータス名
	private Timestamp ORDER_DATETIME;	//貸出日
	private int item_count; 			//個数
	private int max; 					//合計数量
	private int tax; 					//合計金額
	//private int total; 					//集計************
	/*
	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
*/
	public int getTax() {
		return tax;
	}

	public void setTax(int tax) {
		this.tax = tax;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getItem_price() {
		return item_price;
	}

	public void setItem_price(int item_price) {
		this.item_price = item_price;
	}

	public int getItem_count() {
		return item_count;
	}

	public void setItem_count(int item_count) {
		this.item_count = item_count;
	}

	public Timestamp getORDER_DATETIME() {
		return ORDER_DATETIME;
	}

	public void setORDER_DATETIME(Timestamp oRDER_DATETIME) {
		ORDER_DATETIME = oRDER_DATETIME;
	}
/*
	public String getStatus_name() {
		return status_name;
	}

	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}
*/
	public Date getRETURN_DATETIME() {
		return RETURN_DATETIME;
	}

	public void setRETURN_DATETIME(Date rETURN_DATETIME) {
		this.RETURN_DATETIME = rETURN_DATETIME;
	}

	public int getRENTAL_NUMBER() {
		return RENTAL_NUMBER;
	}

	public void setRENTAL_NUMBER(int rENTAL_NUMBER) {
		this.RENTAL_NUMBER = rENTAL_NUMBER;
	}

	public String getSTATUS_NAME() {
		return STATUS_NAME;
	}

	public void setSTATUS_NAME(String sTATUS_NAME) {
		this.STATUS_NAME = sTATUS_NAME;
	}

	public String getItem_name() {
		return item_name;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public String getRecommended_flg() {
		return recommended_flg;
	}

	public void setRecommended_flg(String recommended_flg) {
		this.recommended_flg = recommended_flg;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public String getGenre_name() {
		return genre_name;
	}

	public void setGenre_name(String genre_name) {
		this.genre_name = genre_name;
	}

	public String getNew_and_old_name() {
		return new_and_old_name;
	}

	public void setNew_and_old_name(String new_and_old_name) {
		this.new_and_old_name = new_and_old_name;
	}

	public int getItem_id() {
		return item_id;
	}

	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
}
