package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import test.DataSourceManager;
import test.ItemDAO;
import test.ItemDTO;

/**
 * Servlet implementation class PagingServlet
 */
@WebServlet("/paging")
public class PagingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		// ページ番号を取得
		String pageStr = req.getParameter("page");
		
		// セッションから検索情報を取得
		HttpSession session = req.getSession();
		String searchCategoryStr = (String) session.getAttribute("searchCategory");
		String searchGenreStr = (String) session.getAttribute("searchGenre");
		String searchString = (String) session.getAttribute("searchString");
		int page;
		if(searchCategoryStr == null){
			searchCategoryStr = "0";
		}
		if(searchGenreStr == null){
			searchGenreStr = "0";
		}
		if(searchString == null){
			searchString = "";
		}
		if(pageStr == null || Integer.parseInt(pageStr) < 1){
			pageStr = "1";
			page = 1;
		}else{
			page = Integer.parseInt(pageStr);
		}
		
		// リクエストスコープに現在のページ番号を保存
		req.setAttribute("page", page);
		
		// 返却データ用のリスト
		ArrayList<ItemDTO> list = new ArrayList<>();
		
		try(Connection con = DataSourceManager.getConnections()){
			ItemDAO dao = new ItemDAO(con);
			
			// 指定の条件で検索を行う
			list = dao.searchItems(searchCategoryStr, searchGenreStr, searchString, page);
			
			// セッションに結果を保存
			session.setAttribute("searchResult", list);
			session.setAttribute("backTo", "paging?page=" + pageStr);
			session.setAttribute("returnTo", "paging?page=" + pageStr);
			
			// ビューへ転送
			req.getRequestDispatcher("/searchItem.jsp").forward(req, res);

		}catch(SQLException | NamingException e){
			// エラーページへ転送
			//req.getRequestDispatcher("/error.jsp").forward(req, res);
		}
	}
	
	/**
	 * @throws IOException 
	 * @throws ServletException 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException{
		doGet(req,res);
	}

}
