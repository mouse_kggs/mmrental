package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import test.DataSourceManager;
import test.ItemDAO;
import test.ItemDTO;

/**
 * Servlet implementation class SearchItemServlet
 */
@WebServlet("/searchItem")
public class SearchItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		// パラメータの取得
		req.setCharacterEncoding("UTF-8");
		String searchCategoryStr = req.getParameter("category");
		String searchGenreStr = req.getParameter("genre");
		String searchString = req.getParameter("searchString");
		String pageStr = req.getParameter("page");
		int page;
		if(searchCategoryStr == null){
			searchCategoryStr = "0";
		}
		if(searchGenreStr == null){
			searchGenreStr = "0";
		}
		if(searchString == null){
			searchString = "";
		}
		if(pageStr == null || Integer.parseInt(pageStr) < 1){
			pageStr = "1";
			page = 1;
		}else{
			page = Integer.parseInt(pageStr);
		}
		
		// セッションに検索情報を保存
		HttpSession session = req.getSession();
		session.setAttribute("searchCategory", searchCategoryStr);
		session.setAttribute("searchGenre", searchGenreStr);
		session.setAttribute("searchString", searchString);
		session.setAttribute("backTo", "searchItem?category=" + searchCategoryStr + "&genre=" + searchGenreStr + "&searchString=" + searchString);
		
		// リクエストスコープに現在のページ番号を保存
		req.setAttribute("page", page);
		
		
		// 返却データ用のリスト
		ArrayList<ItemDTO> list = new ArrayList<>();
		
		try(Connection con = DataSourceManager.getConnections()){
			ItemDAO dao = new ItemDAO(con);
			
			// データ件数を取得する
			int dataNum = dao.getItemNum(searchCategoryStr, searchGenreStr, searchString);
			
			// 指定の条件で検索を行う
			list = dao.searchItems(searchCategoryStr, searchGenreStr, searchString, page);
			
			// セッションに結果を保存
			session.setAttribute("searchResult", list);
			session.setAttribute("pageMax", (int)((dataNum + 9) / 10) );
			session.setAttribute("backTo", "searchItem");
			session.setAttribute("returnTo", "searchItem");
			// ビューへ転送
			req.getRequestDispatcher("/searchItem.jsp").forward(req, res);

		}catch(SQLException | NamingException e){
			// エラーページへ転送
			//req.getRequestDispatcher("/error.jsp").forward(req, res);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		//doGet(req,res);
		// パラメータの取得
		req.setCharacterEncoding("UTF-8");
		String searchCategoryStr = req.getParameter("category");
		String searchGenreStr = req.getParameter("genre");
		String searchString = req.getParameter("searchString");
		String pageStr = req.getParameter("page");
		int page;
		if(searchCategoryStr == null){
			searchCategoryStr = "0";
		}
		if(searchGenreStr == null){
			searchGenreStr = "0";
		}
		if(searchString == null){
			searchString = "";
		}
		if(pageStr == null || Integer.parseInt(pageStr) < 1){
			pageStr = "1";
			page = 1;
		}else{
			page = Integer.parseInt(pageStr);
		}
		
		// セッションに検索情報を保存
		HttpSession session = req.getSession();
		session.setAttribute("searchCategory", searchCategoryStr);
		session.setAttribute("searchGenre", searchGenreStr);
		session.setAttribute("searchString", searchString);
		session.setAttribute("backTo", "searchItem?category=" + searchCategoryStr + "&genre=" + searchGenreStr + "&searchString=" + searchString);
		session.setAttribute("returnTo", "searchItem?category=" + searchCategoryStr + "&genre=" + searchGenreStr + "&searchString=" + searchString);

		// リクエストスコープに現在のページ番号を保存
		req.setAttribute("page", page);
		
		
		// 返却データ用のリスト
		ArrayList<ItemDTO> list = new ArrayList<>();
		
		try(Connection con = DataSourceManager.getConnections()){
			ItemDAO dao = new ItemDAO(con);
			
			// データ件数を取得する
			int dataNum = dao.getItemNum(searchCategoryStr, searchGenreStr, searchString);
			
			// 指定の条件で検索を行う
			list = dao.searchItems(searchCategoryStr, searchGenreStr, searchString, page);
			
			// セッションに結果を保存
			session.setAttribute("searchResult", list);
			session.setAttribute("pageMax", (int)((dataNum + 9) / 10) );
			
			// ビューへ転送
			req.getRequestDispatcher("/searchItem.jsp").forward(req, res);

		}catch(SQLException | NamingException e){
			// エラーページへ転送
			//req.getRequestDispatcher("/error.jsp").forward(req, res);
		}
	}

}
