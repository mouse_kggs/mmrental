package test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//
/**
 * Servlet implementation class EditUserConfirmServlet
 */
@WebServlet("/editUserConfirm")
public class EditUserConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.getRequestDispatcher("/homeView").forward(request, response);
	}

	
	boolean errorCheck(String[] error){
		for(int i = 0; i < 21; i++){
			if(error[i] != null){
				return true;
			}
		}
		return false;
		}
	
	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
	
		UserDTO dto = new UserDTO();
		

		
		String[] error = null;
	
		
		//editのときはmailチェックだけ少し違うので、
		//mailチェックを編集した別のcheckDataをつくる？？
		
	    try {
			error = EditUserCheck.checkUserData(req, dto);
		} catch (SQLException | NamingException e1) {
			
			e1.printStackTrace();
		}
		

		
		System.out.println("checked");
		// DTO 
		HttpSession session = req.getSession();
		session.setAttribute("UserData", dto);
		req.setAttribute("msg", error);
		
		System.out.println(error + "error");

		//error[] 
		
		System.out.println(errorCheck(error));
		if(errorCheck(error)){
			//エラーがあれば戻す
			req.getRequestDispatcher("/editUser.jsp").forward(req, res);;
		}else{
	
			try(Connection con = DataSourceManager.getConnections()){
				
				UserDAO dao = new UserDAO(con);
				int userId = dto.getUser_id();
				
				//ユーザー情報を更新
				if(dao.updateUser(dto, userId) && dao.updateCard(dto, userId)){
						
					System.out.println(userId + "u");
						
						//名前も変更されるかもしれないので、改めてユーザー情報を取得し、セッションにセット					
						UserDTO userProfile = new UserDTO();
						//データベースからとってくる
						//userProfile = dao.userProfile(dto.getMail(), dto.getPassword());
						
						//入力フォームからからとってくる
						userProfile.setUser_id(dto.getUser_id());
						userProfile.setUser_name(dto.getUser_name());
						userProfile.setPoint(dto.getPoint());
						
						req.getSession().removeAttribute("UserData");
						session.setAttribute("userProfile", userProfile);
						//HomeViewServlet縺ｫ驕ｷ遘ｻ
						req.getRequestDispatcher("homeView").forward(req, res);
						
					
				} else {
					
					req.setAttribute("message", "更新できませんでした");
					req.getRequestDispatcher("/editUser.jsp").forward(req, res);
				}
				
			} catch (SQLException | NamingException e) {
				
				//req.setAttribute("message", "SQLエラー");
				System.out.println("sqlエラー");
				req.getRequestDispatcher("/editUser.jsp").forward(req, res);
			}
			
		}
		
	
	}

}


