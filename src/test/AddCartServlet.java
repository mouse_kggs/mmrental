package test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
/**
 * Servlet implementation class addCartServlet
 */
@WebServlet("/addCart")
public class AddCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
	
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		//エラーメッセージを削除
		req.removeAttribute("CartError");
		
		//パラメーターで、「カートに追加する」ボタンを押した商品のidをとってくる
				int itemId = Integer.parseInt(req.getParameter("item_id"));
				
				//セッションなければつくる
				HttpSession session = req.getSession(false);
				ArrayList<ItemDTO> itemList = null; 
				
				// 戻る先を取得
			    String returnTo = (String)session.getAttribute("returnTo");
			    System.out.println(returnTo);
			    if(returnTo == null){
			    	returnTo = "/itemDetail";
			    }
				
				//セッションに"itemList"のキーがあれば、とってくる。なければ作る
				if(session.getAttribute("itemList") == null){
					itemList = new ArrayList<>(); 
					session.setAttribute("itemList", itemList);
				} else {
				    itemList = (ArrayList<ItemDTO>) session.getAttribute("itemList");	
				}
		        
				// 初期化
				ItemDTO item = null;
				
				
				//itemListは商品idと数量だけを保持したDTOを内包するList型
			    //同じ商品IDが既にDTOとして保持されているなら、amountフィールドを1追加する
				//6/29 違う商品なのに追加されないバグ
			    for(ItemDTO dto : itemList){
			    	if((itemId == dto.getItem_id()) && (dto.getAmount() < 10)){
			    		dto.setAmount(dto.getAmount() + 1);
			    		req.getRequestDispatcher(returnTo).forward(req, res);
			       		return;
			       	//itemIdが同じかつ数量が既に10以上
			    	}else if ((itemId == dto.getItem_id()) && (dto.getAmount() >= 10)){
			    		//エラーメッセージをセット
			    		req.setAttribute("CartError","同じ商品は11個以上カートに追加できません");
			    		req.getRequestDispatcher(returnTo).forward(req, res);
			    		return;
			    	}
			      	
			    }
				
		         //idがないなら、そのIDを用いて、DTOを作成する。数量は１をセット。
		    	item = new ItemDTO();
		    	item.setItem_id(itemId);
		    	item.setAmount(1);
		    	itemList.add(item);
		    
			    
			    
			    session.setAttribute("itemList", itemList);
			    
			    req.getRequestDispatcher(returnTo).forward(req, res);
			    
	}
}
