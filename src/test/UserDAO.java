package test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

//
public class UserDAO {

	private Connection con = null;

	public UserDAO(Connection con) {
		this.con = con;
	}


	// loginServlet
	public UserDTO userProfile(String mail, String password) throws SQLException {

		// SQL
		StringBuffer sb = new StringBuffer();
		sb.append("select user_id, user_name, point, case when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 20");
		sb.append("       then 10");
		sb.append("       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 30");
		sb.append("       then 20");
		sb.append("       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 40");
		sb.append("       then 30");
		sb.append("       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) < 50");
		sb.append("       then 40");
		sb.append("       when ((year(curdate()) - year(birthday)) - (right(curdate(), 5) < right(birthday, 5))) >= 50");
		sb.append("       then 50");
		sb.append("  end as 'AGE' from user where mail = ? and password = password(?);");
				
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {

			ps.setString(1, mail);
			ps.setString(2, password);

		
			ResultSet rs = ps.executeQuery();

			UserDTO userProfile = new UserDTO();
			if (rs.next()) {
				userProfile.setUser_id(rs.getInt("USER_ID"));
				userProfile.setUser_name(rs.getString("USER_NAME"));
				userProfile.setPoint(rs.getInt("POINT"));
				userProfile.setAge(rs.getInt("AGE"));
			} else {
				return null;
			}			
			return userProfile;

		} catch (SQLException e) {
			throw e;
		}
	}


	// 
	public boolean insertUserData(UserDTO user) throws SQLException {

		StringBuffer sb = new StringBuffer();

		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        USER(USER_NAME,");
		sb.append("        	 MAIl, ");
		sb.append("        	 PASSWORD, ");
		sb.append("        	 BIRTHDAY, ");
		sb.append("        	 TEL, ");
		sb.append("        	 POSTAL_CODE, ");
		sb.append("        	 ADDRESS1, ADDRESS2)");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("            ,password(?)");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("        );");

		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {

			// ?に値をセ?ƒ?トして実?¡?
			ps.setString(1, user.getUser_name());
			ps.setString(2, user.getMail());
			ps.setString(3, user.getPassword());
			ps.setDate(4, user.getBirthday());
			ps.setInt(5, Integer.parseInt(user.getTel()));
			ps.setInt(6, Integer.parseInt(user.getPostal_code()));
			ps.setString(7, user.getAddress1());
			ps.setString(8, user.getAddress2());

			int result = ps.executeUpdate();

			// 成功すればtrueを返す
			if (result == 1) {
				return true;
			}

			// 失敗なのでfalseを返す
			return false;

		} catch (SQLException e) {
			throw e;
		}
	}

	// カード情報のみ登録 引数にはカード情報をå???Œ?したDTOと、userProfileの二つもら? ?
	public boolean insertCreditData(UserDTO card, int userId) throws SQLException {

		StringBuffer sb = new StringBuffer();

		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        CREDITCARD(");
		sb.append("            CARD_ID");
		sb.append("            ,CARD_NUM");
		sb.append("            ,CARD_NAME");
		sb.append("            ,SECURE_CODE");
		sb.append("            ,EXPIRATION_DATE_YEAR");
		sb.append("            ,EXPIRATION_DATE_DAY");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("        )");

		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {

			// ?に値をセ?ƒ?トして実?¡?
			System.out.println(userId + "ユーザーIDを使ってカード登録");
			ps.setInt(1, userId);

			// 三é??演算子でもし""ならNULLにする
			if ("".equals(card.getCard_num())) {
				System.out.println("!");
			}
			
			ps.setString(2, card.getCard_num());
			ps.setString(3, card.getCard_name());
			ps.setString(4, card.getSecure_code());
			ps.setString(5, card.getExpiredate1());
			ps.setString(6, card.getExpiredate2());

			int result = ps.executeUpdate();

			// 成功すればtrueを返す
			if (result == 1) {
				return true;
			}

			// 失敗なのでfalseを返す
			return false;

		} catch (SQLException e) {
			throw e;
		}
	}

	// メールアドレスが重?¤?するかど? ?かã??チェ?ƒ?クするメソ?ƒ??ƒ?
	public boolean mailCheck(String mail) throws SQLException {

		// SQL?–?の作æ??
		StringBuffer sb = new StringBuffer();

		sb.append("SELECT");
		sb.append("        user_id");
		sb.append("    FROM");
		sb.append("        USER");
		sb.append("    WHERE");
		sb.append("        mail = ?;");

		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {

			ps.setString(1, mail);

			// SQLの実?¡?
			ResultSet rs = ps.executeQuery();

			// ?ƒ?ータを?¸?件ずつEmpDTOの形にしてリストへ追?Š?
			if (rs.next()) {
				// 値が存在する
				return true;
			} else {
				// 値が存在しな? ?
				return false;
			}

		} catch (SQLException e) {
			throw e;
		}
	}

	
	
		// ユーザー?ƒ?報の編?›?
		public UserDTO editUser(int userId) throws SQLException {

			// SQL?–?の作æ??
			StringBuffer sb = new StringBuffer();

			sb.append("SELECT");
			sb.append("        t1.user_name");
			sb.append("        ,t1.mail");
			sb.append("        ,t1.password");
			sb.append("        ,t1.birthday");
			sb.append("        ,t1.tel");
			sb.append("        ,t1.postal_code");
			sb.append("        ,t1.address1");
			sb.append("        ,t1.address2");
			sb.append("        ,t2.card_num");
			sb.append("        ,t2.card_name");
			sb.append("        ,t2.secure_code");
			sb.append("        ,t2.expiration_date_year");
			sb.append("        ,t2.expiration_date_day");
			sb.append("    FROM");
			sb.append("        user AS t1");
			sb.append("            LEFT OUTER JOIN creditcard AS t2");
			sb.append("                ON (t1.user_id = t2.card_id)");
			sb.append("    WHERE");
			sb.append("        user_id = ?");
			

			try (PreparedStatement ps = con.prepareStatement(sb.toString())) {

				ps.setInt(1, userId);

				// SQLの実?¡?
				ResultSet rs = ps.executeQuery();

				// ?ƒ?ータを?¸?件ずつEmpDTOの形にしてリストへ追?Š?
				UserDTO data = new UserDTO();
				if (rs.next()) {
					data.setUser_name(rs.getString("USER_NAME"));
					data.setMail(rs.getString("MAIL"));
					data.setPassword(rs.getString("PASSWORD"));
					data.setBirthday(rs.getDate("BIRTHDAY"));
					data.setTel(String.valueOf(rs.getInt("TEL")));
					data.setPostal_code(String.valueOf(rs.getInt("POSTAL_CODE")));
					data.setAddress1(rs.getString("ADDRESS1"));
					data.setAddress2(rs.getString("ADDRESS2"));
					data.setCard_num(rs.getString("CARD_NUM"));
					data.setCard_name(rs.getString("CARD_NAME"));
					data.setSecure_code(rs.getString("SECURE_CODE"));
					data.setExpiredate1(rs.getString("Expiration_date_year"));
					data.setExpiredate2(rs.getString("Expiration_date_day"));
					
					
				} else {
					return null;
				}
				// リストを返す
				return data;

			} catch (SQLException e) {
				throw e;
			}
		}

		
		
		
		// ?? [ƒU [?????̍X V
		public boolean updateUser(UserDTO dto, int userId) throws SQLException {
			
			// SQL???̍쐬
			StringBuffer sb = new StringBuffer();
			sb.append("UPDATE");
			sb.append("        USER");
			sb.append("    SET");
			sb.append("        USER_NAME = ?");
			sb.append("        ,MAIL = ?");
			sb.append("        ,BIRTHDAY = ?");
			sb.append("        ,TEL = ?");
			sb.append("        ,POSTAL_CODE = ?");
			sb.append("        ,ADDRESS1 = ?");
			sb.append("        ,ADDRESS2 = ?");
			if(!"".equals(dto.getPassword())){
				sb.append("        ,PASSWORD = PASSWORD(?)");
			}
			sb.append("    WHERE");
			sb.append("        USER_ID = ?");

			try(PreparedStatement ps = con.prepareStatement(sb.toString())){
				
				// ??ɒl??ƒZƒbƒg
				ps.setString(1, dto.getUser_name());
				ps.setString(2, dto.getMail());
				ps.setDate(3, dto.getBirthday());
				ps.setInt(4, Integer.parseInt(dto.getTel()));
				ps.setInt(5, Integer.parseInt(dto.getPostal_code()));
				ps.setString(6, dto.getAddress1());
				ps.setString(7, dto.getAddress2());
				
				if(!"".equals(dto.getPassword())){
					ps.setString(8, dto.getPassword());
					ps.setInt(9, userId);
				}else{
					ps.setInt(8, userId);
				}
								
				con.setAutoCommit(false);

				int result = ps.executeUpdate();

				if(result == 1){
					con.commit();
					return true;
				}
				
				con.rollback();
				return false;
				
			} catch (SQLException e) {
				throw e;
			}
		}
		
		
		
		
		public boolean updateCard(UserDTO dto, int userId) throws SQLException {
			
			// SQL???̍쐬
			StringBuffer sb = new StringBuffer();
			sb.append("UPDATE");
			sb.append("        CREDITCARD");
			sb.append("    SET");
			sb.append("        CARD_NUM = ?");
			sb.append("        ,CARD_NAME = ?");
			sb.append("        ,SECURE_CODE =?");
			sb.append("        ,EXPIRATION_DATE_YEAR = ?");
			sb.append("        ,EXPIRATION_DATE_DAY = ?");
			sb.append("    WHERE");
			sb.append("        CARD_ID = ?");

			try(PreparedStatement ps = con.prepareStatement(sb.toString())){
				
				// ??ɒl??ƒZƒbƒg
				ps.setString(1, dto.getCard_num());
				ps.setString(2, dto.getCard_name());
				ps.setString(3, dto.getSecure_code());
				ps.setString(4, dto.getExpiredate1());
				ps.setString(5, dto.getExpiredate2());
				ps.setInt(6, userId);
				
				// ƒg????ƒUƒNƒV??????ŠJŽn???Ď? s
				con.setAutoCommit(false);

				int result = ps.executeUpdate();

				// ????????????Ԃ·
				if(result == 1){
					con.commit();
					return true;
				}
				
				// ???Ԃ·
				con.rollback();
				return false;
				
			} catch (SQLException e) {
				throw e;
			}
		}

	
	
	
	

}