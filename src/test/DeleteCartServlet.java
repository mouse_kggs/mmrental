package test;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
/**
 * Servlet implementation class DeleteCartServlet
 */
@WebServlet("/deleteCart")
public class DeleteCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		
		//パラメーターで取得
		int itemId = Integer.parseInt(req.getParameter("item_id"));
		
		//セッションあれば取得
		HttpSession session = req.getSession(false);
		
		//まずセッションに保持していたカート内の商品リストをとりだす
		ArrayList<ItemDTO> items =  (ArrayList<ItemDTO>) session.getAttribute("itemList");
		
//		for(ItemDTO item : items){
//			//パラメータでとってきたIDに該当する商品DTOをArrayListから削除
//			if(item.getItem_id() == itemId){
//				items.remove(item);
//			}
//			
//		}
		
		int i = 0;
		for (ItemDTO dto : new ArrayList<ItemDTO>(items)) {
		    if (dto.getItem_id() == itemId) {
		        items.remove(i);
		    }
		    else {
		         i++;
		    }
		}
		
		req.getRequestDispatcher("/showCart").forward(req, res);



	}

		protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
			//パラメーターで取得
			int itemId = Integer.parseInt(req.getParameter("item_id"));
			
			//セッションあれば取得
			HttpSession session = req.getSession(false);
			
			//まずセッションに保持していたカート内の商品リストをとりだす
			ArrayList<ItemDTO> items =  (ArrayList<ItemDTO>) session.getAttribute("itemList");
			
//			for(ItemDTO item : items){
//				//パラメータでとってきたIDに該当する商品DTOをArrayListから削除
//				if(item.getItem_id() == itemId){
//					items.remove(item);
//				}
//				
//			}
			
			
			
			int i = 0;
			for (ItemDTO dto : new ArrayList<ItemDTO>(items)) {
			    if (dto.getItem_id() == itemId) {
			        items.remove(i);
			    }
			    else {
			         i++;
			    }
			}
			
			req.getRequestDispatcher("/showCart").forward(req, res);
	
	}

}
