package test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
/**
 * Servlet implementation class AddUserConfirmServlet
 */
@WebServlet("/addUserConfirm")
public class AddUserConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
		
		
		
		//error
		boolean errorCheck(String[] error){
		for(int i = 0; i < 21; i++){
			if(error[i] != null){
				return true;
			}
		}
		return false;
		}
		


		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
				
				UserDTO dto = new UserDTO();
				
				
				// 
				
				String[] error = null;
			
			    try {
					error = UserCheck.checkUserData(req, dto);
				} catch (SQLException | NamingException e1) {
					
					e1.printStackTrace();
				}
				
		
				
				System.out.println("checked");
				// DTO
				HttpSession session = req.getSession();
				session.setAttribute("UserData", dto);
				req.setAttribute("msg", error);
				
				System.out.println(error + "error");

				//error[] 
				
				System.out.println(errorCheck(error));
				if(errorCheck(error)){
					//
					req.getRequestDispatcher("/addUser.jsp").forward(req, res);;
				}else{
					// 
					
					// 
					try(Connection con = DataSourceManager.getConnections()){
						
						UserDAO dao = new UserDAO(con);
						UserDTO userProfile = new UserDTO();
						
						if(dao.insertUserData(dto)){
						
						
							userProfile = dao.userProfile(dto.getMail(), dto.getPassword());
							
//							System.out.println(userProfile.getUser_id() + "縺ｯid");
							
							//int userId = userProfile.getUser_id();
							int userId = (int) userProfile.getUser_id();
							
							//
							if(dao.insertCreditData(dto, userId)){
								
								
								req.getSession().removeAttribute("UserData");
								req.removeAttribute("birthday");
								
								
								//
								session.setAttribute("userProfile", userProfile);
								
								//カート用の
								ArrayList itemList = new ArrayList(); 
								session.setAttribute("itemList", itemList);
								
								req.getRequestDispatcher("homeView").forward(req, res);
								
							}	
							
						} else {
							//
							req.setAttribute("message","");
							req.getRequestDispatcher("/Error.jsp").forward(req, res);
						}
						
					} catch (SQLException | NamingException e) {
						
						req.getRequestDispatcher("/jsp").forward(req, res);
					}
					
				}
			}
	}

	