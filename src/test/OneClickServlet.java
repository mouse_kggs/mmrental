package test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Pattern;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//

@WebServlet("/oneClick")
public class OneClickServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	
	/**
	 * 
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
	
		//セッションなければnull
				HttpSession session = req.getSession(false);
				
				req.removeAttribute("error");
				
				//セッションからuserProfileを取り出す
				UserDTO userProfile = (UserDTO) session.getAttribute("userProfile");
				int userId = userProfile.getUser_id();
				
				//パラメーターで支払方法を取得
				String payment="";
				String pchoise = "";
				
				req.setCharacterEncoding("UTF-8");
				
				//日付表示
				java.util.Calendar cal = java.util.Calendar.getInstance();
				DateFormat df = new SimpleDateFormat("yyyy'年'MM'月'dd'日'");
				java.util.Date nowDate = new java.util.Date();		
				cal.setTime(nowDate);
				
		        ArrayList dates = new ArrayList(); 
		        
		        for(int i = 0; i < 7; i++){
		        	cal.add(Calendar.DAY_OF_MONTH, 1);
		        		dates.add(df.format(cal.getTime()));
		        }
		        
		       
		        req.setCharacterEncoding("UTF-8");
				req.setAttribute("dates", dates);
				
				
				//パラメーターチェック
				if(req.getParameter("credit") != null){
					payment = req.getParameter("credit");
				}
				
				if(req.getParameter("point") != null){
					payment = req.getParameter("point");
				}
				
				if(req.getParameter("credit_point") != null){
					payment = req.getParameter("credit_point");
				}
				
				//
				if(req.getParameter("pchoise") != null){
					pchoise = req.getParameter("pchoise");
				}
				String textPoint = req.getParameter("textPoint");
				
				
				//復元するために、届け日時のID　支払い方法　レンタルするアイテムを取得
				int date = Integer.parseInt(req.getParameter("delivery_date"));
				int time = Integer.parseInt(req.getParameter("delivery_time"));
				
				req.setAttribute("date", date );
				req.setAttribute("time", time);
				req.setAttribute("p", pchoise);
								
			

				//商品idと値段取り出す
				int itemId = Integer.parseInt(req.getParameter("item_id"));
				int itemPrice = Integer.parseInt(req.getParameter("price"));
			    
				//もしポイントを使うなら int p につめる
				int p = 0;
				
				try(Connection con = DataSourceManager.getConnections()){
					
					RentalDAO dao = new RentalDAO(con);
					
					
					
					String[] error = new String[4];
									
					//支払方法クレジットのエラー
					if(payment.equals("クレジットでレンタル")){	
						if(!dao.cardCheck(userId)){
							error[0] = "クレジットカード情報を登録してください";
							req.setAttribute("error",error);
							req.getRequestDispatcher("/itemDetail.jsp").forward(req, res);
							return;
						}
					}
					
					//支払方法allポイントに関するエラー
					 if(payment.equals("ポイントでレンタル") && (itemPrice > userProfile.getPoint())){
						 error[1] = "ポイントが不足しています";
						req.setAttribute("error",error);
						req.getRequestDispatcher("/itemDetail.jsp").forward(req, res);				
						return;
					 }
					 
					
					
					
					 
					if(payment.equals("クレジット＆ポイントでレンタル")){	
							if(!dao.cardCheck(userId)){
								error[2] = "クレジットカード情報を登録してください";
								req.setAttribute("error",error);
								req.getRequestDispatcher("/itemDetail.jsp").forward(req, res);
								return;
							}
					}
					 
										 
					//29日　変更‥
					if(pchoise.equals("some") && payment.equals("クレジット＆ポイントでレンタル") && !Pattern.compile("^[1-9][0-9]*$").matcher(textPoint).matches()){
						error[3] = "有効なポイント数を入力してください";
						req.setAttribute("error",error);
						req.getRequestDispatcher("/itemDetail.jsp").forward(req, res);
						return;
					}


									
					//クレジット＆ポイントで、一部ポイント使用に関するエラー
					 if(pchoise.equals("some") && payment.equals("クレジット＆ポイントでレンタル")){
						 if(textPoint.equals("") || (Integer.parseInt(textPoint) > userProfile.getPoint()) || (Integer.parseInt(textPoint) > itemPrice)){
							 error[3] = "有効なポイント数を入力してください";
								req.setAttribute("error",error);
							 req.getRequestDispatcher("/itemDetail.jsp").forward(req, res);
							 return;
						 }
					 }
			
				
					
				
					if(dao.InsertRental (userProfile, date, time)){
							
							//注文受付番号を取得
							int rentalNo  = dao.getRentalNo(userId);
							if(rentalNo == 0){
								System.out.println("失敗");
								return;
							}
							if(dao.InsertRentalDetail(rentalNo, itemId, itemPrice)){
								session.setAttribute("rentalNo", rentalNo);
							}							
												
							///差し引くポイントを計算 
							//ポイントを全部使うなら
							if(payment.equals("ポイントでレンタル") ||  payment.equals("クレジット＆ポイントでレンタル") && pchoise.equals("all")){
								p = userProfile.getPoint();
								//pointをマイナスする
								PointDAO Pdao = new PointDAO(con);
								//ポイントを差し引いたぶん、userProfileにセットし直す
								if(p > itemPrice){
									userProfile.setPoint(p - itemPrice);
								}else{
									userProfile.setPoint(0);
								}
								
								Pdao.updatePoint(userProfile.getPoint(), userProfile.getUser_id());
								res.sendRedirect("rentalSuccess.jsp");
								
							//ポイントを一部だけ使ったなら
			       			}else if(payment.equals("クレジット＆ポイントでレンタル") && pchoise.equals("some")){
			       				p = Integer.parseInt(textPoint);
			       				PointDAO Pdao = new PointDAO(con);
								//ポイントを差し引いたぶん、userProfileにセットし直す
								userProfile.setPoint(userProfile.getPoint() - p);
								Pdao.updatePoint(userProfile.getPoint(), userProfile.getUser_id());
								res.sendRedirect("rentalSuccess.jsp");
							}else if(payment.equals("クレジットでレンタル")){
								res.sendRedirect("rentalSuccess.jsp");
			       			}
					}
					
					}catch (SQLException | NamingException e) {
						
							//req.setAttribute("message", "SQLerro");
							System.out.println("sqlerro");
							req.getRequestDispatcher("/ad.jsp").forward(req, res);
				 }
					 	
				
			}

}