package test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


//

public class PointDAO {
	
	private Connection con = null;

	public PointDAO(Connection con) {
		this.con = con;
	}
	
	
   //ポイントで購入後、ポイントチャージ後にupdateする
	public boolean updatePoint(int point, int userId) throws SQLException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        USER");
		sb.append("    SET");
		sb.append("        point = ?");
		sb.append("    WHERE");
		sb.append("        user_id = ?;");

 
		try(PreparedStatement ps = con.prepareStatement(sb.toString())){
			
			// ?に値をセット
			ps.setInt(1, point);
			ps.setInt(2, userId);
				
			// トランザクションを開始して実行
			con.setAutoCommit(false);

			int result = ps.executeUpdate();

			// 成功すればコミットしtrueを返す
			if(result == 1){
				con.commit();
				return true;
			}
			
			// 失敗なのでロールバックしてfalseを返す
			con.rollback();
			return false;
			
		} catch (SQLException e) {
			throw e;
		}
	}
	
	
	//　ポイントチャージする額
	public boolean insertPcharge(int userId, int point) throws SQLException {

		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        PCHARGE(");
		sb.append("            USER_ID");
		sb.append("            ,CHARGEDATE");
		sb.append("            ,CHARGEVAL");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,current_timestamp()");
		sb.append("            ,?");
		sb.append("        );");


		
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {

			
			ps.setInt(1, userId);
			ps.setInt(2, point);

			int result = ps.executeUpdate();

			// 成功
			if (result == 1) {
				return true;
			}

			// 
			return false;

		} catch (SQLException e) {
			throw e;
		}
	}
	
	
	
}
