package test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import test.DataSourceManager;
import test.UserDAO;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// ログイン画面に遷移

		req.getRequestDispatcher("homeView").forward(req, res);
		
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// ログイン画面の入力値を取得する
		req.setCharacterEncoding("UTF-8");
		String mail = req.getParameter("mail");
		String password = req.getParameter("password");

		// セッション取得
		HttpSession session = req.getSession();
		
		// ログインの場合
		try(Connection con = DataSourceManager.getConnections()) {
			UserDAO dao = new UserDAO(con);

			System.out.println("ok mail->" + mail + " pass->" + password);

			// ユーザーのメールとパスワードが正しく、DBからユーザー情報が取得できている（空でない）場合 → trueになる
			if (dao.userProfile(mail, password) != null) {
				UserDTO user = new UserDTO();
				user = dao.userProfile(mail, password);

				// セッションに ユーザーの名前、所有ポイント、IDを保持
				session.setAttribute("userProfile", user);

				// セッションにログインユーザ保存
				//session.setAttribute("user", mail);
				
				//エラーメッセージ削除
				req.removeAttribute("Error");
				
				// ホーム画面一覧サーブレットに転送
				req.getRequestDispatcher("homeView").forward(req, res);
				
			} else {
				// ログイン画面に転送
				System.out.println("login failed");
				req.setAttribute("Error", "メールアドレス又はパスワードが間違っています");
				doGet(req,res);
				//return;
			}
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
			req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, res);
		}
	}
}
