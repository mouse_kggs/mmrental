package test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//
@WebServlet("/homeView")
public class HomeViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
   
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{			
			// ログイン画面に遷移
		
		//urlをたたくなら セッションを必ずつくる？
		HttpSession session = req.getSession(false);
			if(session == null){ 
				session = req.getSession();
			}else if(session.getAttribute("userProfile") != null){
				doPost(req, res);
				return;
			}
			
			
			//会員登録画面の入力情報をリセット
			session.removeAttribute("UserData");
				
			try(Connection con = DataSourceManager.getConnections()){

				ArrayList<ItemDTO> recommendItems = new ArrayList<>();				
				ItemDAO dao = new ItemDAO(con);
				
				//お勧めを10件取得する
				recommendItems = dao.getRecommendItems(10);
				
				//スコープに値を保持
				req.setAttribute("recommendItems", recommendItems);			
				
				req.removeAttribute("error");
				req.getRequestDispatcher("/home.jsp").forward(req, res);
			
			}catch (SQLException | NamingException e) {
				e.printStackTrace();
				req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, res);
			}							
	}

	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		HttpSession session = req.getSession(false);
		if(session.getAttribute("userProfile") == null){
			doGet(req, res);
		    return;	
		}
		
		//セッションにはユーザー情報を保持している。getUserIdメソッドを用いてユーザーIDだけとりだす。
	    //	int userId =  ((UserDTO) session.getAttribute("userProfile")).getUserId();
		
		
		int userId = ((UserDTO) session.getAttribute("userProfile")).getUser_id();
		
		try(Connection con = DataSourceManager.getConnections()){
		
			ArrayList<ItemDTO> rentalItems = new ArrayList<>();
			ArrayList<ItemDTO> recommendItems = new ArrayList<>();
				
			ItemDAO dao = new ItemDAO(con);
			
			//userIDに紐づいた過去のレンタル商品を最大5件取得する
			rentalItems = dao.getRentalItems(userId);
		
			//おすすめを5件取得
			recommendItems = dao.getRecommendItems(5);
			
			//スコープに値を保持
			req.setAttribute("recommendItems", recommendItems);	
			
			//スコープに値を保持
			req.setAttribute("rentalItems", rentalItems);			
			
			// session.removeAttribute("error_message");
			req.getRequestDispatcher("/home.jsp").forward(req, res);
		
		}
		catch (SQLException | NamingException e) {
			e.printStackTrace();
			req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, res);
		}			
	}
}
