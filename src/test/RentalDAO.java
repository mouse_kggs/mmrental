package test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
//
public class RentalDAO {

	private Connection con = null;
	
	public RentalDAO(Connection con) {
		this.con = con;
	}
	
	
	// レンタル表にInsertする
	public boolean InsertRental (UserDTO userProfile, int Delivery_date_Id , int Delivery_time_Id) throws SQLException{
	
	
				StringBuffer sb = new StringBuffer();
				sb.append("INSERT");
				sb.append("    INTO");
				sb.append("        rental(");
				sb.append("            USER_ID");
				sb.append("            ,ORDER_DATETIME");
				sb.append("            ,DELIVERY_DATE");
				sb.append("            ,DELIVERY_TIME_ID");
				sb.append("            ,STATUS_ID");				
				sb.append("            ,age");
				sb.append("        )");
				sb.append("    VALUES");
				sb.append("        (");
				sb.append("            ?");
				sb.append("            ,current_timestamp()");
				sb.append("            ,date_add(");
				sb.append("                now()");
				sb.append("                ,INTERVAL ? day");
				sb.append("            )");
				sb.append("            ,?");
				sb.append("            ,1");
				sb.append("            ,?");
				sb.append("        );");
			
				
				try(PreparedStatement ps = con.prepareStatement(sb.toString())){
					
					
					// ?に値をセット
					ps.setInt(1, userProfile.getUser_id());
					ps.setInt(2, Delivery_date_Id);
					ps.setInt(3, Delivery_time_Id);
					ps.setInt(4, userProfile.getAge());
					
					// トランザクションを開始して実行
					con.setAutoCommit(false);
			
					int result = ps.executeUpdate();
			
					// 成功すればコミットしtrueを返す
					if(result == 1){
						//まだコミットしない。
						//con.commit();
						return true;
					}
					
					// 失敗なのでロールバックしてfalseを返す
					con.rollback();
					return false;
					
				} catch (SQLException e) {
					throw e;
				}
			}

	
	
			//注文受付番号を取得する  失敗時はゼロを返す
		public int getRentalNo(int userId) throws SQLException {
			
			int rentalNo;
			// SQL
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        rental_number");
			sb.append("    FROM");
			sb.append("        rental");
			sb.append("    WHERE");
			sb.append("        user_id = ?");
			sb.append("    ORDER BY");
			sb.append("        order_datetime DESC limit 0");
			sb.append("        ,1;");

	
			try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
	
				ps.setInt(1, userId);
				
				ResultSet rs = ps.executeQuery();
	
				if (rs.next()) {
				 rentalNo = rs.getInt("RENTAL_NUMBER");
				 return rentalNo;
				} else {
					// 失敗なのでロールバックしてfalseを返す
					con.rollback();
				}			
				return 0;
	
			} catch (SQLException e) {
				throw e;
			}
	}
	
		
		
		// 注文受付番号と商品ID, 値段をレンタル詳細表にぶちこむ
		public boolean InsertRentalDetail (int rentalNo, int itemId, int itemPrice) throws SQLException{
			
				
				StringBuffer sb = new StringBuffer();
				sb.append("INSERT");
				sb.append("    INTO");
				sb.append("        rental_detail(");
				sb.append("            rental_number");
				sb.append("            ,item_id");
				sb.append("            ,price");
				sb.append("        )");
				sb.append("    VALUES");
				sb.append("        (");
				sb.append("            ?");
				sb.append("            ,?");
				sb.append("            ,?");
				sb.append("        );");
		
				try(PreparedStatement ps = con.prepareStatement(sb.toString())){
					  
					
					// ?に値をセット
					ps.setInt(1, rentalNo);
					ps.setInt(2, itemId);
					ps.setInt(3, itemPrice);
		
					int result = ps.executeUpdate();
			
					// 成功すればコミットしtrueを返す
					if(result == 1){
						//ここではじめてコミット
						con.commit();
						return true;
					}
					
					// 失敗なのでロールバックしてfalseを返す
					con.rollback();
					return false;
					
				} catch (SQLException e) {
					throw e;
				}
			}
	
		
		  // userIDに紐づくカード番号があるかチェック
			public boolean cardCheck(int userId) throws SQLException {
			
				StringBuffer sb = new StringBuffer();
				sb.append(" SELECT");
				sb.append("            card_num");
				sb.append("        FROM");
				sb.append("            creditcard");
				sb.append("        WHERE");
				sb.append("            card_id = ?;");
						
			try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
	
				ps.setInt(1, userId);
				
				ResultSet rs = ps.executeQuery();
	
				if (rs.next()) {
					//クレジット番号はvarchar型で保持
					//テスト前は　nullと比較していましたが、空文字に変更しました
				   if(!"".equals(rs.getString("card_num"))){
					   return true;
				   } 			
				
				}return false;
				
			} catch (SQLException e) {
				throw e;
			}
	}

		
		
		
		
}