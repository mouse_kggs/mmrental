package test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//
/**
 * Servlet implementation class ItemDetailServlet
 */
@WebServlet("/itemDetail")
public class ItemDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		

		
		//日付表示
		java.util.Calendar cal = java.util.Calendar.getInstance();
		DateFormat df = new SimpleDateFormat("yyyy'年'MM'月'dd'日'");
		java.util.Date nowDate = new java.util.Date();		
		cal.setTime(nowDate);
		
        ArrayList dates = new ArrayList(); 
        
        for(int i = 0; i < 7; i++){
        	cal.add(Calendar.DAY_OF_MONTH, 1);
        		dates.add(df.format(cal.getTime()));
        }
        
        for(int i = 0; i < 7; i++){
            System.out.println(dates.get(i));
        }
        
        req.setCharacterEncoding("UTF-8");
		req.setAttribute("dates", dates);
		
		
		
		
		int itemId = Integer.parseInt(req.getParameter("item_id"));
		
		try(Connection con = DataSourceManager.getConnections()){
	
				ItemDAO dao = new ItemDAO(con);
				ItemDTO detail = new ItemDTO();
			
				//商品idを渡して、商品詳細の情報をげっと
				detail = dao.getItemDetail(itemId);
				
				//sessionにおかなきゃエラーの時再表示するとき見れない
				HttpSession session = req.getSession();
			    session.setAttribute("detail", detail);
	  
			    
				req.getRequestDispatcher("/itemDetail.jsp").forward(req, res);
			
	
		}catch (SQLException | NamingException e) {
			e.printStackTrace();
			req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, res);
		}
		
		
		
		
	}

	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
