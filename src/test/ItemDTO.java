package test;

import java.sql.Date;
//
public class ItemDTO {

	
	private int item_id;
	private String item_name ;
	private String category_name;
	private String genre_name;
	private String recommended_flg;
	private String new_and_old_name;
	private String image;
	private String artist;
	private int price;
	private String remarks;
	private int amount;
	
	
    //数量を保持させる 
 	public int getAmount() {
		return amount;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	
	
	
	public String getItem_name() {
		return item_name;
	}
	
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	
	public String getRecommended_flg() {
		return recommended_flg;
	}
	
	public void setRecommended_flg(String recommended_flg) {
		this.recommended_flg = recommended_flg;
	}
	
	
	public String getImage() {
		return image;
	}
	
	public void setImage(String image) {
		this.image = image;
	}
	
	public String getArtist() {
		return artist;
	}
	
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public String getGenre_name() {
		return genre_name;
	}

	public void setGenre_name(String genre_name) {
		this.genre_name = genre_name;
	}

	public String getNew_and_old_name() {
		return new_and_old_name;
	}

	public void setNew_and_old_name(String new_and_old_name) {
		this.new_and_old_name = new_and_old_name;
	}

	public int getItem_id() {
		return item_id;
	}

	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	
	
	
	
	
}
