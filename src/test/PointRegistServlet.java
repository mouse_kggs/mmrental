package test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.regex.Pattern;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//
@WebServlet("/pointRegist")
public class PointRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
	
		try(Connection con = DataSourceManager.getConnections()){
			
		HttpSession session = req.getSession(false);
		int userId = ((UserDTO) session.getAttribute("userProfile")).getUser_id();
		int pointNow = ((UserDTO) session.getAttribute("userProfile")).getPoint();
		
		//パラメーターで値をとってくる
		String[] pointCharge = req.getParameterValues("charge");
		
		
		String code = req.getParameter("code");
		
		//エラーを取り除く
		req.removeAttribute("chargeError");
		
		
		//英数字でない場合
		// (Pattern.compile("^[0-9a-zA-Z]+$").matcher(code).matches()){
			
		//ここは16行以下？
		if(code != "" && code.length() == 16 && pointCharge[0] != "" && pointCharge[0] != null && Pattern.compile("^[0-9a-zA-Z]+$").matcher(code).matches()){
			//コードが空ならエラーで返す
			int charge = Integer.parseInt(pointCharge[0]);
			PointDAO dao = new PointDAO(con);
			   //user表には合計ポイントをアップデート                          
			if(dao.updatePoint(charge + pointNow , userId) &&  dao.insertPcharge(userId, charge) ){
				
				//現在のuserProfileに保持されているポイントに追加する
				((UserDTO) session.getAttribute("userProfile")).setPoint(pointNow += charge);
				
				req.setAttribute("success", pointCharge[0] + "ポイント追加しました");
				
				String returnTo = (String)session.getAttribute("returnTo");
				if(returnTo == null){
					returnTo = "/pointCharge.jsp";
				}
				System.out.println(returnTo);
				req.getRequestDispatcher(returnTo).forward(req, res);
			}
			
		}else if(pointCharge[0] == null){
			req.setAttribute("Error","チャージするポイントを選択してください");
			req.getRequestDispatcher("/pointCharge.jsp").forward(req, res);
		}else {
		req.setAttribute("selected", pointCharge[0]);
		req.setAttribute("chargeError","正しいWebMoneyコードを入力してください");
		req.getRequestDispatcher("/pointCharge.jsp").forward(req, res);
		
		
		}	
		
		
		}catch (SQLException | NamingException e) {
			
			//req.setAttribute("message", "SQLエラー");
			System.out.println("sqlエラー");
			req.getRequestDispatcher("/error.jsp").forward(req, res);
		}
	}
		
}
