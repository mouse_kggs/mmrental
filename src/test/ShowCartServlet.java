package test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//
/**
 * Servlet implementation class ShowCartServlet
 */
@WebServlet("/showCart")
public class ShowCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		try(Connection con = DataSourceManager.getConnections()){

			ItemDAO dao = new ItemDAO(con);
			
			//sessionに保持していたitemList(カート内に追加されているitem_idを内包しているリスト型)を取り出す
			HttpSession session = req.getSession(false);
			ArrayList<ItemDTO> itemList = (ArrayList<ItemDTO>) session.getAttribute("itemList");
			
			
			ArrayList<ItemDTO> cartItemList = null;
			//item_idを用いてカート内商品をゲット
			//itemListは、カートに入っている商品の数量と商品idのみ保持したDTOを内包したリスト型
			//CartItemListは詳細データも保持している
			 cartItemList = dao.getItemDetails(itemList);
			
			 
			 
			 //商品データを保持　　もしrequestscopeだと 
			 // ログイン前にカートについかしたやつが、ログイン後 cartItemListが取得できずみれなくなったのでsessionに保持
			 session.setAttribute("cartItemList", cartItemList);
			 
			 
			 
			 //商品データの数量の合計、金額の合計も保持
			 int price = 0;
			 int amount = 0;
			 for(ItemDTO dto : cartItemList){
				 price += dto.getPrice() * dto.getAmount();
				 amount += dto.getAmount();
			 }
			 
			 req.setAttribute("TotalPrice", price);
			 req.setAttribute("TotalAmount", amount);
	  
	 
			 req.getRequestDispatcher("/showCart.jsp").forward(req, res);
		
	
		}catch (SQLException | NamingException e) {
			e.printStackTrace();
			req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, res);
		}
	}
		

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
	
			    
		try(Connection con = DataSourceManager.getConnections()){
		
			ItemDAO dao = new ItemDAO(con);
			
			//sessionに保持していたitemList(カート内に追加されているitem_idを内包しているリスト型)を取り出す
			HttpSession session = req.getSession(false);
			ArrayList<ItemDTO> itemList = (ArrayList<ItemDTO>) session.getAttribute("itemList");
			
			ArrayList<ItemDTO> cartItemList = null;
			
			for(ItemDTO dto : itemList){
				System.out.println(dto.getAmount());
			}
			
			//item_idを用いてカート内商品をゲット
			 cartItemList = dao.getItemDetails(itemList);
			 
			 
			 //商品データの数量の合計、金額の合計も保持
			 int price = 0;
			 int amount = 0;
			 for(ItemDTO dto : cartItemList){
				 price += dto.getPrice() * dto.getAmount();
				 amount += dto.getAmount();
			 }
			 
			 session.setAttribute("TotalPrice", price);
			 
			 session.setAttribute("TotalAmount", amount);
			
			 //商品データを保持
			 session.setAttribute("cartItemList", cartItemList);
	  
			 req.getRequestDispatcher("/showCart.jsp").forward(req, res);
		
	
		}catch (SQLException | NamingException e) {
			e.printStackTrace();
			req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, res);
		}
	}
}