package test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//
/**
 * Servlet implementation class EditUserServlet
 */
@WebServlet("/editUser")
public class EditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
	
		
		try(Connection con = DataSourceManager.getConnections()){
			
			//
			HttpSession session = req.getSession();
			int userId = ((UserDTO) session.getAttribute("userProfile")).getUser_id();
	
			UserDAO dao = new UserDAO(con);
			
			//userID
			UserDTO data = dao.editUser(userId);
			
			//
			req.setAttribute("UserData", data);	
			
			String birthday;
			 
		    if(data.getBirthday() != null){
		    	// 誕生日が保存されているなら'/'区切りに直す
				System.out.println("birthday");
				Date date =  data.getBirthday();	
				birthday = date.toString().replace('-','/');
				req.setAttribute("birthday", birthday);
				
			}
		    //メールアドレスも別に保存
		    session.setAttribute("oldmail", data.getMail()); 
		    req.setAttribute("oldMail",data.getMail());
		    
			// session.removeAttribute("error_message");
			req.getRequestDispatcher("/editUser.jsp").forward(req, res);
		
		}
		catch (SQLException | NamingException e) {
			e.printStackTrace();
			req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, res);
		}			
	
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}