package test;

import java.sql.Date;

public class UserDTO {

	//
		private int user_id;
		private String user_name;
		private String mail;
		private String password;
		private Date birthday;
		private int age;
		//電話番号（tel）はデータベース上ではvarcharで保持されてたから
		private String tel; 
		private String postal_code;
		private String address1;
		private String address2;
		private String card_num;
		private String card_name;
		
		// データベース上の有効期限は　varchar? date型？　もしvarchar型ならここでもString型で保持
		private String expiredate1;
		private String expiredate2;
		private String secure_code;
		private int point;
		private String mailCheck;
		private String passwordCheck;
 		
		public UserDTO(){
			
		}
		
		public int getUser_id() {
			return user_id;
		}
		
		public void setUser_id(int userId) {
			this.user_id = userId;
		}
		
		public String getUser_name() {
			return user_name;
		}

		public void setUser_name(String userName) {
			this.user_name = userName;
		}

		public String getMail() {
			return mail;
		}

		public void setMail(String mail) {
			this.mail = mail;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public Date getBirthday() {
			return birthday;
		}

		public void setBirthday(Date birthday) {
			this.birthday = birthday;
		}

		public String getPostal_code() {
			return postal_code;
		}

		public void setPostal_code(String postcode) {
			this.postal_code = postcode;
		}

		public String getAddress1() {
			return address1;
		}

		public void setAddress1(String address1) {
			this.address1 = address1;
		}

		public String getAddress2() {
			return address2;
		}

		public void setAddress2(String address2) {
			this.address2 = address2;
		}

		public String getCard_num() {
			return card_num;
		}

		public void setCard_num(String cardnum) {
			this.card_num = cardnum;
		}

		public String getCard_name() {
			return card_name;
		}

		public void setCard_name(String cardname) {
			this.card_name = cardname;
		}

		public String getExpiredate1() {
			return expiredate1;
		}

		public void setExpiredate1(String expiredate1) {
			this.expiredate1 = expiredate1;
		}

		public String getExpiredate2() {
			return expiredate2;
		}

		public void setExpiredate2(String expiredate2) {
			this.expiredate2 = expiredate2;
		}

		public String getSecure_code() {
			return secure_code;
		}


		public void setSecure_code(String secure_code) {
			this.secure_code = secure_code;

		}

		public int getPoint() {
			return point;
		}
		
		public void setPoint(int point) {
			this.point = point;
		}

		public int getAge() {
			return age;
		}
		
		public void setAge(int age){
			this.age = age;
		}

		public void setTel(String tel) {
			this.tel = tel;
		
		}
		public String getTel(){
			return tel;
		}

		public String getMailCheck() {
			return mailCheck;
		}

		public void setMailCheck(String mailCheck) {
			this.mailCheck = mailCheck;
		}

		public String getPasswordCheck() {
			return passwordCheck;
		}

		public void setPasswordCheck(String passwordCheck) {
			this.passwordCheck = passwordCheck;
		}

		
}