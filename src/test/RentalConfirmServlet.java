package test;
//
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Pattern;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/rentalConfirm")
public class RentalConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
	}

	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		

	    //日付表示
		java.util.Calendar cal = java.util.Calendar.getInstance();
		DateFormat df = new SimpleDateFormat("yyyy'年'MM'月'dd'日'");
		java.util.Date nowDate = new java.util.Date();		
		cal.setTime(nowDate);
		
        ArrayList dates = new ArrayList(); 
        
        for(int k = 0; k < 7; k++){
        	cal.add(Calendar.DAY_OF_MONTH, 1);
        		dates.add(df.format(cal.getTime()));
        }
        
        
        req.setCharacterEncoding("UTF-8");
		req.setAttribute("dates", dates);
	
		
		//セッションなければnull
		HttpSession session = req.getSession(false);
		//セッションからidとageを取り出す
		int userId = ((UserDTO) session.getAttribute("userProfile")).getUser_id();
		
		//セッションからuserProfileを取り出す
		UserDTO userProfile = (UserDTO) session.getAttribute("userProfile");
		//合計金額
		int totalPrice = (int) session.getAttribute("TotalPrice");
		
		try(Connection con = DataSourceManager.getConnections()){
			
			RentalDAO dao = new RentalDAO(con);
			//パラメーターで支払方法を取得
			String payment = req.getParameter("payment");
			
			String pchoise = "";
			if(req.getParameter("pchoise") != null){
				pchoise = req.getParameter("pchoise");
			}
			String textPoint = req.getParameter("textPoint");
			
			
			//エラー時に復元するために、お届け日時のID　支払い方法　レンタルするアイテムを取得
			int date = Integer.parseInt(req.getParameter("delivery_date"));
			int time = Integer.parseInt(req.getParameter("delivery_time"));
		
			req.setAttribute("date", date );
			req.setAttribute("time", time);
			req.setAttribute("choose", payment);
			req.setAttribute("p", pchoise);
			
			//もしポイントを使うなら int p につめる
			int p = 0;
			req.removeAttribute("error");
			
			String[] error = new String[5];
			//
			if(payment == null){
				error[0] = "支払い方法を選択してください";
				req.setAttribute("error", error);
				req.getRequestDispatcher("/deliveryConfirm.jsp").forward(req, res);
				return;
			}
			
			
			//支払方法クレジットのエラー
			if(payment.equals("credit") || payment.equals("credit_point")){	
				if(!dao.cardCheck(userId)){
					error[1] = "クレジットカード情報を登録してください";
					req.setAttribute("error",error);
					req.getRequestDispatcher("/deliveryConfirm.jsp").forward(req, res);
					return;
				}
			}
			
				//支払方法allポイントに関するエラー
			 if(payment.equals("point") && (totalPrice > userProfile.getPoint())){
				 error[2] = "ポイントが不足しています";
				req.setAttribute("error",error);
				req.getRequestDispatcher("/deliveryConfirm.jsp").forward(req, res);				
				return;
			 }
			 
			//クレジット＆ポイントで、一部ポイント使用に関するエラー
			 if(pchoise.equals("some") && payment.equals("credit_point")) {
				 if(textPoint.equals("") || (Integer.parseInt(textPoint) > userProfile.getPoint()) ||
					(Integer.parseInt(textPoint) > totalPrice) || !Pattern.compile("^[1-9][0-9]*$").matcher(textPoint).matches()){
					 error[3] = "有効なポイント数を入力してください";
						req.setAttribute("error",error);
					 req.getRequestDispatcher("/deliveryConfirm.jsp").forward(req, res);
					 return;
				 }
			 }
		
			
			if(dao.InsertRental (userProfile, date, time)){
					
					//注文受付番号を取得
					int rentalNo  = dao.getRentalNo(userId);
					if(rentalNo == 0){
						System.out.println("失敗");
						return;
					}
					
					//セッションからカートに入っている商品のDTOリストをゲット
					ArrayList<ItemDTO> cartItemList = (ArrayList<ItemDTO>) session.getAttribute("cartItemList");
					
					for(ItemDTO item : cartItemList){
						if(dao.InsertRentalDetail(rentalNo, item.getItem_id(), item.getPrice())){
						}else{
							System.out.println("一部商品のみレンタル成功");
						}
					 //	
					}session.setAttribute("rentalNo", rentalNo);
					
					//カート内アイテムを削除
					session.removeAttribute("itemAmount");
					session.removeAttribute("cartItemList");
					session.removeAttribute("itemList");
					
					//購入した合計金額をtotalCostに代入
					int totalCost = 0;
					for (ItemDTO dto : cartItemList){
						totalCost += dto.getAmount() * dto.getPrice();
					}  
										
					///差し引くポイントを計算 
					//ポイントを全部使うなら
					if(payment.equals("point") ||  payment.equals("credit_point") && pchoise.equals("all")){
						p = userProfile.getPoint();
						//pointをマイナスする
						PointDAO Pdao = new PointDAO(con);
						if(p > totalCost){
							userProfile.setPoint(p - totalCost);
						}else{
							userProfile.setPoint(0);
						}
						//ポイントを差し引いたぶん、userProfileにセットし直す
						Pdao.updatePoint(userProfile.getPoint(), userProfile.getUser_id());
						res.sendRedirect("rentalSuccess.jsp");
					//ポイントを一部だけ使ったなら
	       			}else if(payment.equals("credit_point") && pchoise.equals("some")){
	       				p = Integer.parseInt(textPoint);
	       				PointDAO Pdao = new PointDAO(con);
						//ポイントを差し引いたぶん、userProfileにセットし直す
						userProfile.setPoint(userProfile.getPoint() - p);
						Pdao.updatePoint(userProfile.getPoint(), userProfile.getUser_id());
						res.sendRedirect("rentalSuccess.jsp");
					}else if(payment.equals("credit")){
						res.sendRedirect("rentalSuccess.jsp");
	       			}
			}
			
			}catch (SQLException | NamingException e) {
				
					//req.setAttribute("message", "SQLerro");
					System.out.println("sqlerro");
					req.getRequestDispatcher("/ad.jsp").forward(req, res);
		 }
			 	
		
	}

}