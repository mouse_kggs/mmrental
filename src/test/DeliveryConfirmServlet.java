
package test;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
/**
 * Servlet implementation class DeliveryConfirmServlet
 */
@WebServlet("/deliveryConfirm")
public class DeliveryConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.sendRedirect("/homeView");
		}

	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		

		//セッションなければつくる
		HttpSession session = req.getSession(false);
		//セッションの中にuserProfileがなければ、エラーメッセージを表示
		/// 後で書く　session.getAttribute("userProfile");
		
		// 戻ってくる先を保存しておく
		session.setAttribute("returnTo", "/deliveryConfirm");
	
		   
        req.setCharacterEncoding("UTF-8");
		
		//getParameterで数量をすべてとってくる
		String itemAmount[] = req.getParameterValues("amount");
		
		
		//sessionに入っているカートの商品リストをとりだす
		ArrayList<ItemDTO> cartItemList = (ArrayList<ItemDTO>) session.getAttribute("cartItemList");
		
		int i = 0;
		if(itemAmount == null){
			itemAmount = (String[])session.getAttribute("itemAmount");
		}
		for(ItemDTO dto : cartItemList){
			//パラメーターでとってきた値で、　数量を更新する
			dto.setAmount(Integer.parseInt(itemAmount[i]));
			i++;
		}
				 
		 //showCartServlet上で計算した　商品データの数量の合計、金額の合計を更新
		 int price = 0;
		 int amount = 0;
		 for(ItemDTO dto : cartItemList){
			 price += dto.getPrice() * dto.getAmount();
			 amount += dto.getAmount();
		 }
		 
		    //日付表示
			java.util.Calendar cal = java.util.Calendar.getInstance();
			DateFormat df = new SimpleDateFormat("yyyy'年'MM'月'dd'日'");
			java.util.Date nowDate = new java.util.Date();		
			cal.setTime(nowDate);
			
	        ArrayList dates = new ArrayList(); 
	        
	        for(int k = 0; k < 7; k++){
	        	cal.add(Calendar.DAY_OF_MONTH, 1);
	        		dates.add(df.format(cal.getTime()));
	        }
	        
	 
			req.setAttribute("dates", dates);
		
			session.setAttribute("itemAmount", itemAmount);
			session.setAttribute("TotalPrice", price);
			session.setAttribute("TotalAmount", amount);
	
			
			req.getRequestDispatcher("/deliveryConfirm.jsp").forward(req, res);
	}
	
	
}
