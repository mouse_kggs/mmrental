package test;
//

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

public class UserCheck {

	

	@SuppressWarnings("deprecation")
	public static String[] checkUserData(HttpServletRequest req, UserDTO dto) throws SQLException, NamingException, UnsupportedEncodingException{
		

		String data;
		int dataInt;
		
		String[] error = new String[21];
		
		req.setCharacterEncoding("UTF-8");
		// User NAME
		data = req.getParameter("name");
		if(data != null && data.length() <= 20 && data.length() > 0){
			// OK
			dto.setUser_name(data);
		}else if(data.length() > 20){
			// 20文字以内でない場合
			error[0] = "氏名は20文字以内で入力してください";
		}else{
			// 0もしくは null
			dto.setUser_name("");
			error[1] = "氏名は必須項目です";
		}
		
		// 	mail
		data = req.getParameter("mail");
		
		//メールが既に存在するかチェック
		try(Connection con = DataSourceManager.getConnections()){
			
			UserDAO dao = new UserDAO(con);
			if(dao.mailCheck(data)){
				dto.setMail(data);
				error[2] = "入力されたメールアドレスは既に登録されています";
			}
			
		}
			
		if(data != null && data.length() <= 90 && data.length() > 0){
			// OK
			dto.setMail(data);
		}else if(data.length() > 90){
			// 90文字以内でない
			error[3] = "メールアドレスは90文字以内で入力してください";
		}else{
			// 0もしくはnull
			dto.setMail("");
			error[4] = "メールアドレスは必須項目です";
		}
		
		if(!data.equals(req.getParameter("mailCheck"))){
			error[5] = "正しいメールアドレスを入力してください";
		}
		
		//	passsword
		data = req.getParameter("password");
		if(data != null && data.length() <= 20 && data.length() > 0 && data.length() >= 6){
			// OK
			dto.setPassword(data);
		}else if(data.length() > 20){
			error[6] = "パスワードは20文字以内で入力してください";
		}else if(data.length() < 6){
			// 20文字以内でない
			error[6] = "パスワードは6文字以上入力してください";
		}else{
			// 0またはnull
			dto.setPassword("");
			error[7] = "パスワードは必須項目です";
		}

		if(!data.equals(req.getParameter("passwordCheck"))){
			error[8] = "正しいメールアドレスを入力してください";
		}

	
		// 生年月日
		data = req.getParameter("birthday");
		if(data != null){
			SimpleDateFormat format = new SimpleDateFormat("yyyy/mm/dd");
			format.setLenient(false);
			
			try{
				// 
				format.parse(data);
				
				// OK
				String[] s = data.split("/");
				dto.setBirthday(new Date((Integer.parseInt(s[0])-1900), (Integer.parseInt(s[1])-1), Integer.parseInt(s[2])));
				req.setAttribute("birthday", data);
			}catch(ParseException e){
				// 不正な日
				error[9] = "生年月日はYYYY/MM/DDの形式で正しく入力してください";
			}
		}else{
			// 不正な日
			error[10] = "生年月日はYYYY/MM/DDの形式で入力してください";
		}
		
				
		// 電話番号
		data = req.getParameter("tel");
		if(data != "" && data.length() <= 11 && Pattern.compile("\\d+").matcher(data).matches()){
				dto.setTel(data);
		}else if(data.equals("")){
			// 11桁以下の数字でない
			dto.setTel("");
			error[11] = "電話番号は必須項目です";
		}else{
			dto.setTel(data);
			error[11] = "電話番号を正しく入力してください";
		}
		
		
		// 郵便番号
		data = req.getParameter("postal_code");
		if(data != "" && data.length() <= 7 && Pattern.compile("\\d+").matcher(data).matches()){
				dto.setPostal_code(data);
		}else if(data.equals("")){
			// 11桁以下の数字でない
			dto.setTel("");
			error[12] = "郵便番号は必須項目です";
		}else{
			dto.setTel(data);
			error[12] = "郵便番号を正しく入力してください";
		}
		

		// Address1
		data = req.getParameter("address1");
		if(data != null && data.length() <= 50 && data.length() > 0){
			// OK
			dto.setAddress1(data);
		}else if(data.length() > 50){
			// 50文字以上
			dto.setAddress1(data);
			error[13] = "住所１は50文字以下で入力してください";
		}else{
			// 0もしくはnull
			dto.setAddress1("");
			error[14] = "住所１は必須項目です";
		}
		

		// Address2
		data = req.getParameter("address2");
		if(data.length() <= 50 && data.length() > 0){
			// 空でもOK
			dto.setAddress2(data);
		}else if(data.length() > 50){
			// 50文字以上
			dto.setAddress2(data);
			error[13] = "住所2は50文字以下で入力してください";
		}
		
		
	   //全部から
        if("".equals(req.getParameter("card_num")) && "".equals(req.getParameter("card_name")) &&
        	"".equals(req.getParameter("secure_code")) && "".equals(req.getParameter("expiredate1")) && 
        	"".equals(req.getParameter("expiredate2"))){

        	dto.setCard_num(req.getParameter("card_num"));
        	dto.setCard_name(req.getParameter("card_name"));
        	dto.setSecure_code(req.getParameter("secure_code"));
        	dto.setExpiredate1(req.getParameter("expiredate1"));
        	dto.setExpiredate2(req.getParameter("expiredate2"));
        	
        
		}else{
			//カード番号
			data = req.getParameter("card_num");
			if(data.length() <= 16 && data.length() > 0){
				// OK
				dto.setCard_num(data);
			}else if("".equals(data)){
				// 空?–??­?
				dto.setCard_num("");
				error[16] = "カード番号を正しく入力してください";
			}
	
			

			//card_name
			data = req.getParameter("card_name"); //全て半角英字かチェック
			if(data.length() <= 20 && data.length() > 0 && Pattern.compile("^[a-zA-Z]+$").matcher(data).matches()){
			// ok
				dto.setCard_name(data);
			}else{
				//　から文字
				dto.setCard_name(data);
				error[17] = "カード名義人を正しく入力してください";
			}
						
			
			//セキュリティ
			data = req.getParameter("secure_code");
			if(data.length() <= 3 && data.length() > 0){
				// OK
				dto.setSecure_code(data);
			}else if("".equals(data)){
				// 
				dto.setSecure_code("");
				error[18] = "セキュリティコードを正しく入力してください";
			}
			
			String month = "";
			String year = "";
			
			// 有効期限
			data = req.getParameter("expiredate1");
			if(data != null && data.length() <= 2){
				switch(data){
				case "17":
				case "18":
				case "19":
				case "20":
				case "21":
				case "22":
					
					// OK
					
					dto.setExpiredate1(data);
					year = data;
					break;
				default:
					// NG
					error[19] = "有効期限を正しく入力してください";
				}
			}
			
			data = req.getParameter("expiredate2");
			if(data != null && data.length() <= 2){
				switch(data){
				case "1":					
				case "2":
				case "3":
				case "4":
				case "5":
				case "6":
				case "7":
				case "8":
				case "9":					
				case "10":
				case "11":
				case "12":
					// OK
					dto.setExpiredate2(data);
					month = data;
					break;
					default:
					// NG
					error[19] = "有効期限を正しく入力してください";
				}
			
				//今日より前の期限はNG
				if(month.equals("1") || month.equals("2") || month.equals("3") || month.equals("4") || month.equals("5")){
					if(year.equals("17")){
						error[19] = "有効期限を正しく入力してください";
					}
				}
			}
		}
		return error;
        
	}
}