package test;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/pointCharge")
public class PointChargeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
//
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
	
		HttpSession session = req.getSession(false);
		if(session == null){
			res.sendRedirect("/homeView");
		}else{
			req.getRequestDispatcher("/pointCharge.jsp").forward(req, res);
		}

	}

	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		
		
		req.getRequestDispatcher("/pointCharge.jsp").forward(req, res);
		
	}

}
