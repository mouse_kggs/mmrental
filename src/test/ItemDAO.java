package test;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

//


public class ItemDAO {


	private Connection con = null;

	public ItemDAO(Connection con) {
		this.con = con;
	}
	
	// SQL文の作成 降順でレンタルIDをとってくる
	// いっぺんに商品情報をとってくる
	public ArrayList<ItemDTO> getRentalItems (int user_id) throws SQLException{
		ArrayList<ItemDTO> list = new ArrayList<>();
	
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT");
		sb.append("        DISTINCT t2.item_id");
		sb.append("        ,t1.rental_number");
		sb.append("        ,t3.item_name");
		sb.append("        ,t6.new_and_old_name");
		sb.append("        ,t3.recommended_flg");
		sb.append("        ,t3.artist");
		sb.append("        ,t3.price");
		sb.append("        ,t3.image");
		sb.append("        ,t3.remarks");
		sb.append("        ,t4.genre_name");
		sb.append("        ,t5.category_name");
		sb.append("    FROM");
		sb.append("        rental AS t1");
		sb.append("            LEFT OUTER JOIN rental_detail AS t2");
		sb.append("                ON (t1.rental_number = t2.rental_number)");
		sb.append("            LEFT OUTER JOIN item AS t3");
		sb.append("                ON (t2.item_id = t3.item_id)");
		sb.append("            LEFT OUTER JOIN genre AS t4");
		sb.append("                ON (t3.genre_id = t4.genre_id)");
		sb.append("            LEFT OUTER JOIN category AS t5");
		sb.append("                ON (t3.category_id = t5.category_id)");
		sb.append("            LEFT OUTER JOIN new_and_old AS t6");
		sb.append("                ON (t3.new_and_old_id = t6.new_and_old_id)");
		sb.append("    WHERE");
		sb.append("        user_id = ?");
		sb.append("    ORDER BY");
		sb.append("        t1.order_datetime DESC limit 0");
		sb.append("        ,5;");

		
		try(PreparedStatement ps = con.prepareStatement(sb.toString())){
			
			// SQLの実行
			ps.setInt(1, user_id);
			ResultSet rs = ps.executeQuery();
			
			// データを一件ずつEmpDTOの形にしてリストへ追加
			while(rs.next()){
				ItemDTO item = new ItemDTO();
				item.setItem_name(rs.getString("ITEM_NAME"));
				item.setNew_and_old_name(rs.getString("new_and_old_name"));
				item.setRecommended_flg(rs.getString("recommended_flg"));
				item.setImage(rs.getString("image"));
				item.setArtist(rs.getString("artist"));
				item.setPrice(rs.getInt("price"));
				item.setRemarks(rs.getString("remarks"));
				item.setGenre_name(rs.getString("genre_name"));
				item.setCategory_name(rs.getString("category_name"));
				item.setItem_id(rs.getInt("item_id"));
		
				
				list.add(item);
			}
			// リストを返す
			return list;
			
		} catch (SQLException e) {
			throw e;
		}
	}
	
	
	public ArrayList<ItemDTO> getRecommendItems(int count) throws SQLException{
		
		ArrayList<ItemDTO> list = new ArrayList<>();
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT");
		sb.append("        t1.item_id");
		sb.append("        ,t1.recommended_flg");
		sb.append("        ,t1.image");
		sb.append("        ,t1.item_name");
		sb.append("        ,t1.artist");
		sb.append("        ,t1.price");
		sb.append("        ,t1.remarks");
		sb.append("        ,t2.genre_name");
		sb.append("        ,t3.category_name");
		sb.append("        ,t4.new_and_old_name");
		sb.append("    FROM");
		sb.append("        item AS t1");
		sb.append("            LEFT OUTER JOIN genre AS t2");
		sb.append("                ON (t1.genre_id = t2.genre_id)");
		sb.append("            LEFT OUTER JOIN category AS t3");
		sb.append("                ON (t1.category_id = t3.category_id)");
		sb.append("            LEFT OUTER JOIN new_and_old AS t4");
		sb.append("                ON (t1.new_and_old_id = t4.new_and_old_id)");
		sb.append("    WHERE");
		sb.append("        recommended_flg = '1'");
		sb.append("    ORDER BY");
		sb.append("        rand() limit 0");
		sb.append("        ,?;");

		

		try(PreparedStatement ps = con.prepareStatement(sb.toString())){
			
			// SQLの実行
			ps.setInt(1, count);
			ResultSet rs = ps.executeQuery();
			
			// データを一件ずつEmpDTOの形にしてリストへ追加
			while(rs.next()){
				ItemDTO item = new ItemDTO();
				item.setItem_id(rs.getInt("ITEM_ID"));
				item.setItem_name(rs.getString("ITEM_NAME"));
				item.setNew_and_old_name(rs.getString("new_and_old_name"));
				item.setRecommended_flg(rs.getString("recommended_flg"));
				item.setImage(rs.getString("image"));
				item.setArtist(rs.getString("artist"));
				item.setPrice(rs.getInt("price"));
				item.setRemarks(rs.getString("remarks"));
				item.setGenre_name(rs.getString("genre_name"));
				item.setCategory_name(rs.getString("category_name"));
		
				list.add(item);
			}
			// リストを返す
			return list;
			
		} catch (SQLException e) {
			throw e;
		}
	}
	
	public ItemDTO getItemDetail (int item_id) throws SQLException{
		ItemDTO item = new ItemDTO();
	
		StringBuffer sb = new StringBuffer();

		sb.append("SELECT");
		sb.append("        t1.image");
		sb.append("        ,t1.item_name");
		sb.append("        ,t1.artist");
		sb.append("        ,t1.price");
		sb.append("        ,t1.remarks");
		sb.append("        ,t2.genre_name");
		sb.append("        ,t3.category_name");
		sb.append("        ,t4.new_and_old_name");
		sb.append("    FROM");
		sb.append("        item AS t1");
		sb.append("            LEFT OUTER JOIN genre AS t2");
		sb.append("                ON (t1.genre_id = t2.genre_id)");
		sb.append("            LEFT OUTER JOIN category AS t3");
		sb.append("                ON (t1.category_id = t3.category_id)");
		sb.append("            LEFT OUTER JOIN new_and_old AS t4");
		sb.append("                ON (t1.new_and_old_id = t4.new_and_old_id)");
		sb.append("    WHERE");
		sb.append("        item_id = ?;");


		
		try(PreparedStatement ps = con.prepareStatement(sb.toString())){
			
			// SQLの実行
			ps.setInt(1, item_id);
			ResultSet rs = ps.executeQuery();
			
			// データを一件ずつEmpDTOの形にしてリストへ追加
			while(rs.next()){
				
				//引数でもらったidもセットしておく
				item.setItem_id(item_id);
				item.setImage(rs.getString("image"));
				item.setItem_name(rs.getString("ITEM_NAME"));
				item.setArtist(rs.getString("artist"));
				item.setPrice(rs.getInt("price"));
				item.setRemarks(rs.getString("remarks"));
				item.setGenre_name(rs.getString("genre_name"));
				item.setCategory_name(rs.getString("category_name"));
				item.setNew_and_old_name(rs.getString("new_and_old_name"));
							
				
			}
			// リストを返す
			return item;
			
		} catch (SQLException e) {
			throw e;
		}
	}
	
	
	//itemIDがはいったリスト型を引数でもらう、
	public ArrayList<ItemDTO> getItemDetails (ArrayList<ItemDTO> itemList) throws SQLException{

		ArrayList<ItemDTO> list = new ArrayList<>();
		
		StringBuffer sb = new StringBuffer();

		sb.append("SELECT");
		sb.append("        t1.image");
		sb.append("        ,t1.item_name");
		sb.append("        ,t1.artist");
		sb.append("        ,t1.price");
		sb.append("        ,t1.remarks");
		sb.append("        ,t2.genre_name");
		sb.append("        ,t3.category_name");
		sb.append("        ,t4.new_and_old_name");
		sb.append("    FROM");
		sb.append("        item AS t1");
		sb.append("            LEFT OUTER JOIN genre AS t2");
		sb.append("                ON (t1.genre_id = t2.genre_id)");
		sb.append("            LEFT OUTER JOIN category AS t3");
		sb.append("                ON (t1.category_id = t3.category_id)");
		sb.append("            LEFT OUTER JOIN new_and_old AS t4");
		sb.append("                ON (t1.new_and_old_id = t4.new_and_old_id)");
		sb.append("    WHERE");
		sb.append("        item_id = ?;");


		
		try(PreparedStatement ps = con.prepareStatement(sb.toString())){
			
			// SQLの実行
			//　for文で回して、idひとつひとつ実行
			
			for(ItemDTO dto : itemList){
				ps.setInt(1, dto.getItem_id());
				ResultSet rs = ps.executeQuery();
				
				// データを一件ずつEmpDTOの形にしてリストへ追加
				while(rs.next()){
					
					ItemDTO item = new ItemDTO();	
					//引数でもらったidもセットしておく
					item.setItem_id(dto.getItem_id());
					item.setAmount(dto.getAmount());
					item.setImage(rs.getString("image"));
					item.setItem_name(rs.getString("ITEM_NAME"));
					item.setArtist(rs.getString("artist"));
					item.setPrice(rs.getInt("price"));
					item.setRemarks(rs.getString("remarks"));
					item.setGenre_name(rs.getString("genre_name"));
					item.setCategory_name(rs.getString("category_name"));
					item.setNew_and_old_name(rs.getString("new_and_old_name"));
					list.add(item);	
					
				}
			}
			// リストを返す
			return list;
			
		} catch (SQLException e) {
			throw e;
		}
	}
	
	public ArrayList<ItemDTO> searchItems(String category,String genre, String searchString, int page) throws SQLException {

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        item_id");
		sb.append("        ,new_and_old_name");
		sb.append("        ,category_name");
		sb.append("        ,item_name");
		sb.append("        ,artist");
		sb.append("        ,price");
		sb.append("    FROM");
		sb.append("        item INNER JOIN new_and_old");
		sb.append("            ON item.new_and_old_id = new_and_old.new_and_old_id INNER JOIN category");
		sb.append("            ON item.category_id = category.category_id");
		sb.append(" WHERE");
		sb.append("    item_name LIKE ?");
		if(!"0".equals(category)){
			sb.append("    AND item.category_id LIKE ?");
		}
		if(!"0".equals(genre)){
			sb.append("    AND item.genre_id LIKE ?");
		}
		sb.append("ORDER BY");
		sb.append("    item_id DESC limit 10 offset ?");
		
		ArrayList<ItemDTO> itemList = new ArrayList<>();
		
		try(PreparedStatement ps = con.prepareStatement(sb.toString())){
			
			// ?に値をセットして実行
			int i = 1;
			ps.setString(i++, "%" + searchString + "%");
			if(!"0".equals(category)){
				ps.setString(i++, category);
			}
			if(!"0".equals(genre)){
				ps.setString(i++, genre);
			}
			ps.setInt(i, (page - 1) * 10);
			ResultSet rs = ps.executeQuery();
			
			// データを一件ずつEmpDTOの形にしてリストへ追加
			while(rs.next()){
				ItemDTO item = new ItemDTO();
				item.setItem_id(rs.getInt("item_id"));
				item.setNew_and_old_name(rs.getString("new_and_old_name"));
				item.setCategory_name(rs.getString("category_name"));
				item.setItem_name(rs.getString("item_name"));
				item.setArtist(rs.getString("artist"));
				item.setPrice(rs.getInt("price"));
				itemList.add(item);
			}
			return itemList;
			
		} catch (SQLException e) {
			throw e;
		}
	}

	public int getItemNum(String category, String genre, String searchString) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        count(*)");
		sb.append("    FROM");
		sb.append("        item INNER JOIN new_and_old");
		sb.append("            ON item.new_and_old_id = new_and_old.new_and_old_id INNER JOIN category");
		sb.append("            ON item.category_id = category.category_id");
		sb.append(" WHERE");
		sb.append("    item_name LIKE ?");
		if(!"0".equals(category)){
			sb.append("    AND item.category_id LIKE ?");
		}
		if(!"0".equals(genre)){
			sb.append("    AND item.genre_id LIKE ?");
		}
		
		try(PreparedStatement ps = con.prepareStatement(sb.toString())){
			
			// ?に値をセットして実行
			int i = 1;
			ps.setString(i++, "%" + searchString + "%");
			if(!"0".equals(category)){
				ps.setString(i++, category);
			}
			if(!"0".equals(genre)){
				ps.setString(i++, genre);
			}
			ResultSet rs = ps.executeQuery();
			
			// 取得したデータの数を返却
			if(rs.next()){
				return rs.getInt(1);
			}
			
			throw new SQLException();
			
		} catch (SQLException e) {
			throw e;
		}
	}
	
	/**
	 * 画像データを取得するメソッド
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public BufferedImage selectImageById(int id) throws Exception {

		try{

			// SQL文を作成する
			StringBuffer sb = new StringBuffer();
			sb.append("   select");
			sb.append("          image");
			sb.append("     from item");
			sb.append("    where item_id = ?");

			// SQL文を実行する
			PreparedStatement ps = con.prepareStatement(sb.toString());
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			// 結果セットから画像データを取得し、返却する
			if (rs.next()) {
				InputStream is = rs.getBinaryStream("image");
				BufferedInputStream bis = new BufferedInputStream(is);
				return ImageIO.read(bis);
			}

		} catch (IOException | SQLException e) {
			//e.printStackTrace();
			throw e;
		}
		return null;
	}

}
