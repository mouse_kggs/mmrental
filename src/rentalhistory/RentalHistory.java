package rentalhistory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import historyDAO.HisDAO;
import historyDTO.HisDTO;
import test.DataSourceManager;
import test.UserDTO;

@WebServlet("/rentalHistory")
public class RentalHistory extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse rsp) throws ServletException, IOException {

		HttpSession session = req.getSession(true);

		try (Connection con = DataSourceManager.getConnections()) {
			HisDAO hisDao = new HisDAO(con);
			
			UserDTO userDto = new UserDTO();
			//ユーザー名、所有ポイント、IDを取得
			userDto = (UserDTO) session.getAttribute("userProfile");

			userDto.getUser_id();
			int n = userDto.getUser_id();

			/*
			 * hissearchをhislistに代入
			 */
			ArrayList<HisDTO> hisList = hisDao.hissearch(n);

			req.setAttribute("hisList1", hisList);
			req.getRequestDispatcher("/rentalHistory.jsp").forward(req, rsp);
		} catch (SQLException | NamingException e) {
			//提出時外す
			e.printStackTrace();
			throw new ServletException(e);
		}
	}
}
